# Welcome

The goal of this app is to record laptimes of Acc sessions, both multiplayer and singleplayer.

This app connects to the UDP stream of the game which should be enabled by editing 

Documents\config\broadcasting.json to 


```
#!java

{
    "updListenerPort": 9000,
    "connectionPassword": "asd",
    "commandPassword": ""
}
```

Installer made with "multi-platform installer builder" https://www.ej-technologies.com/products/install4j/overview.html


[![install4j_large.png](https://bitbucket.org/repo/R57ykad/images/1538872047-install4j_large.png)](https://www.ej-technologies.com/products/install4j/overview.html)
[![btn_donate_SM.gif](https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif)](https://www.paypal.com/donate?business=XCX98P88D8XRE&currency_code=EUR)
https://www.buymeacoffee.com/robbytimmermans


# Features #

* Connect to Assetto Corsa Competitione session and record data per session
* Show live timing with fastest and theoretical best laps per driver
* Browse to previously recorded sessions and get laptimes per driver
* Get the weather and time of day of a lap (this makes it easier to compare laptimes)
* Export complete sessions to pdf
* Export driver laptimes to pdf
* Copy lapinfo to clipboard
* Show fastest sectors per driver

![Screenshot 2021-01-20 at 21.55.03.png](https://bitbucket.org/repo/R57ykad/images/155152065-Screenshot%202021-01-20%20at%2021.55.03.png)

![Screenshot 2021-01-20 at 22.02.50.png](https://bitbucket.org/repo/R57ykad/images/1250998063-Screenshot%202021-01-20%20at%2022.02.50.png)

![Screenshot 2021-01-20 at 22.03.32.png](https://bitbucket.org/repo/R57ykad/images/2778089567-Screenshot%202021-01-20%20at%2022.03.32.png)

![Screenshot 2021-01-20 at 22.05.03.png](https://bitbucket.org/repo/R57ykad/images/3876521820-Screenshot%202021-01-20%20at%2022.05.03.png)