package be.timsys.gaming.acc.recorder.analytics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class AccSessionStartRequest {

    private final String installationId;
}
