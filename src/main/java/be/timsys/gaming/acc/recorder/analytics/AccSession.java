package be.timsys.gaming.acc.recorder.analytics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class AccSession {

    private String sessionId;
    private String message;
}
