package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.export.ExportService;
import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.DateUtils;
import be.timsys.gaming.acc.recorder.ui.export.ShareDialog;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
@RequiredArgsConstructor
public class LapDetail extends VBox {

    public static final double WIDTH = 300.0;
    public static final double HEIGHT = 300.0;
    private final ExportService exportService;
    private final ShareDialog shareDialog;

    private Label rain;
    private Label lapNumber;
    private Label lapTime;
    private Label temp;
    private Label timeOfDay;
    private Label timeRecorded;
    private Label position;
    private Label driver;
    private Button share;

    @PostConstruct
    public void init() {
        setPadding(new Insets(10, 10, 10, 10));
        rain = createLabel();
        temp = createLabel();
        timeOfDay = createLabel();
        timeRecorded = createLabel();
        lapNumber = createLabel();
        lapTime = createLabel();
        lapTime.setStyle("-fx-font-size: 25");
        position = createLabel();
        driver = createLabel();
        share = new Button("Share");

        getChildren().add(position);
        getChildren().add(lapNumber);
        getChildren().add(driver);
        getChildren().add(lapTime);
        getChildren().add(rain);
        getChildren().add(temp);
        getChildren().add(timeOfDay);
        getChildren().add(timeRecorded);
        getChildren().add(share);
    }

    private Label createLabel() {
        var label = new Label();
        label.setPadding(new Insets(0, 0, 7, 0));
        return label;
    }

    public void setLap(Session session, SessionEntry entry, Lap lap) {
        lapNumber.setText("Lap " + lap.getLapNumber());
        lapTime.setText(lap.getFormattedTime());
        position.setText("Position " + lap.getPositionOnLapCompleted());
        if (lap.getWeather() != null) {
            rain.setText(lap.getWeather().getWeatherDescription());
            temp.setText(lap.getWeather().getTempDescription());
        } else {
            rain.setText("");
            temp.setText("");
        }
        if (lap.getGameTimeOfDay() != null) {
            timeOfDay.setText("In game time " + lap.getGameTimeOfDay().getFormattedTime());
        } else {
            timeOfDay.setText("");
        }

        if (lap.getRealRecordedTime() != null) {
            timeRecorded.setText("Recorded " + DateUtils.formatDate(lap.getRealRecordedTime()));
        } else {
            timeRecorded.setText("");
        }

        if (lap.getDriver() != null) {
            var d = lap.getDriver();
            driver.setText(String.format("%s (%s - %s)", d.getDriverFullName(), d.getNationality(), d.getCup()));
        } else {
            driver.setText("");
        }

        share.setOnAction((event -> {
            var url = exportService.exportToImageUrl(session, entry, lap, WIDTH, HEIGHT);
            var image = new Image(url, WIDTH, HEIGHT, true, true, true);

            shareDialog.setImage(image);
            shareDialog.setText(exportService.exportToString(session, entry, lap));
            shareDialog.show();
        }));
    }

}
