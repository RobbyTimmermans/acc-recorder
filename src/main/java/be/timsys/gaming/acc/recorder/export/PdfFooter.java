package be.timsys.gaming.acc.recorder.export;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.property.TextAlignment;
import lombok.SneakyThrows;

public class PdfFooter implements IEventHandler {
    @SneakyThrows
    @Override
    public void handleEvent(Event event) {
        var pdfEvent = (PdfDocumentEvent) event;
        var pageSize = pdfEvent.getPage().getPageSize();

        try (Canvas canvas = new Canvas(pdfEvent.getPage(), pageSize)) {
            canvas.setFont(PdfFontFactory.createFont(StandardFonts.COURIER));
            canvas.setFontSize(7)
                    .showTextAligned("ACC-Recorder by Timsys.be", (float) 20.0, (float) 10.0, TextAlignment.LEFT);
        }
    }
}
