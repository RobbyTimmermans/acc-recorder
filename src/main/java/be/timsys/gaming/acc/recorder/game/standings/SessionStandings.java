package be.timsys.gaming.acc.recorder.game.standings;

import be.timsys.gaming.acc.recorder.game.Driver;
import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.SessionEvent;
import be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType;
import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

@NoArgsConstructor
public class SessionStandings {

    private List<SessionEntry> sessionEntries;

    public void addEntry(SessionEntry sessionEntry) {
        if (sessionEntries == null) {
            sessionEntries = new ArrayList<>();
        }

        if (find(sessionEntry.getId()).isEmpty()) {
            sessionEntries.add(sessionEntry);
        }
    }

    public boolean hasEntries() {
        return sessionEntries != null && sessionEntries.size() > 0;
    }

    public List<SessionEntry> fastest(CarClass carClass) {
        if (sessionEntries != null) {
            return getEntries(carClass).stream().filter(e -> e.getFastestLap() != null).sorted(comparing(SessionEntry::getFastestLap)).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }
    private List<SessionEntry> getEntries(CarClass carClass) {
        if (carClass != null) {
            return sessionEntries.stream().filter(e -> e.getCarClass() == carClass).collect(Collectors.toList());
        } else {
            return sessionEntries;
        }
    }

    public List<SessionEntry> result() {
        if (sessionEntries != null) {
            return sessionEntries.stream()
                    .sorted(comparing(SessionEntry::getNumberOfLaps).reversed().thenComparing(SessionEntry::getPosition))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    public void updatePosition(short carIndex, short newPosition) {
        if (sessionEntries != null) {
            find(carIndex).ifPresent(e -> e.setPosition(newPosition));
        }
    }

    public Optional<SessionEntry> find(short id) {
        return sessionEntries.stream().filter(s -> s.getId() == id).findFirst();
    }

    public List<SessionEntry> getSessionEntries(CarClass carClass) {
        return getEntries(carClass) != null ? sessionEntries : Collections.emptyList();
    }

    public List<SessionEntry> getSessionEntries() {
        return sessionEntries != null ? sessionEntries : Collections.emptyList();
    }

    void addLap(short carId, Lap lap) {
        if (sessionEntries != null) {
            find(carId).ifPresent(e -> e.addLap(lap));
        }
    }

    void addEvent(short carId, SessionEvent sessionEvent) {
        if (sessionEntries != null) {
            find(carId).ifPresent(e -> e.addSessionEvent(sessionEvent));
        }
    }

    void driverChange(short carId, Driver newDriver) {
        if (sessionEntries != null) {
            find(carId).ifPresent(e -> {
                e.setDriver(newDriver.getDriverFullName());
                e.setCurrentDriver(newDriver);
            });
        }
    }

    public List<CarClass> getCarClasses() {
        if (sessionEntries != null) {
            return sessionEntries.stream().map(SessionEntry::getCarClass).filter(Objects::nonNull).distinct().collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public List<SessionEvent> getSessionEvents(BroadCastMessageType ... types) {
        return getSessionEvents((CarClass) null, types);
    }

    public List<SessionEvent> getSessionEvents(CarClass carClass, BroadCastMessageType ... types) {
        if (sessionEntries!= null) {
            sessionEntries.stream().filter(s -> s.getEvents() != null).forEach(this::setAdditionalData);

            return getEntries(carClass).stream().filter(s -> s.getEvents() != null)
                    .map(SessionEntry::getEvents)
                    .flatMap(Collection::stream)
                    .filter(e -> ArrayUtils.contains(types, e.getType()))
                    .sorted(comparing(SessionEvent::getTimeOfDay))
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    // TODO remove method
    private void setAdditionalData(SessionEntry s) {
        s.getEvents().forEach(e -> {
            e.setCar(s.getCar());
            e.setNumber(s.getNumber());
        });
    }
}
