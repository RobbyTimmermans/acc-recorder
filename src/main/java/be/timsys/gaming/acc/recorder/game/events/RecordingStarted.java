package be.timsys.gaming.acc.recorder.game.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.File;

@Getter
@AllArgsConstructor
public class RecordingStarted {

    private final String sessionId;
    private final File dumpFile;
}
