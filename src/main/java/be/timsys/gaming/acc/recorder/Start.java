package be.timsys.gaming.acc.recorder;

import be.timsys.gaming.acc.recorder.analytics.AnalyticsService;
import be.timsys.gaming.acc.recorder.spring.ApplicationConfig;
import be.timsys.javafx.scene.SceneManager;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.stage.Stage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Start extends Application {

    private static AnnotationConfigApplicationContext context;

    public static void main(String[] args) {
        launch(args);
    }

    private void startSpringContext() {
        context = new AnnotationConfigApplicationContext();
        context.registerBean("hostServices", HostServices.class, this::getHostServices);
        context.register(ApplicationConfig.class);
        context.refresh();
    }

    @Override
    public void start(Stage primaryStage) {
        startSpringContext();

        primaryStage.setTitle("ACC - Recorder by Timsys");
        primaryStage.setWidth(1024);
        primaryStage.setHeight(800);

        var analyticsService = (AnalyticsService) context.getBean("analyticsService");
        analyticsService.startSession();
        
        var sceneManager = (SceneManager) context.getBean("sceneManager");
        sceneManager.setPrimaryStage(primaryStage);
        sceneManager.start();

        primaryStage.show();

        primaryStage.setOnCloseRequest(e -> context.close());
    }
}
