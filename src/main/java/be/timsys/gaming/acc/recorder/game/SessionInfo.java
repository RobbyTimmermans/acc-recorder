package be.timsys.gaming.acc.recorder.game;

import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import be.timsys.gaming.acc.recorder.game.acc.Weather;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SessionInfo {

    public String trackName;
    public Integer trackId;
    public String sessionType;
    public String sessionPhase;
    public String sessionIndex;
    public String event;
    public String sessionId;
    public Weather weather;
    public TimeOfDay timeOfDay;
}
