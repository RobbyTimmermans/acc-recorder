package be.timsys.gaming.acc.recorder.session;

import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import be.timsys.gaming.acc.recorder.game.acc.SessionType;
import be.timsys.gaming.acc.recorder.game.standings.SessionStandings;
import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Session {

    @Id
    private String id;
    private OffsetDateTime created;
    private OffsetDateTime lastModified;
    private String trackName;
    private Integer trackId;
    private Integer eventIndex;

    @Enumerated(EnumType.STRING)
    private SessionType sessionType;

    private String fileName;

    @Convert(converter = JSONConverter.class)
    private SessionStandings sessionStandings;

    public List<CarClass> getCarClasses() {
        if (sessionStandings != null) {
            return sessionStandings.getCarClasses();
        } else {
            return null;
        }
    }
}
