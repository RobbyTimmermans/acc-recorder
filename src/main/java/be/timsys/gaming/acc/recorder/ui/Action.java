package be.timsys.gaming.acc.recorder.ui;

public interface Action<S> {

    void execute(S s);

}
