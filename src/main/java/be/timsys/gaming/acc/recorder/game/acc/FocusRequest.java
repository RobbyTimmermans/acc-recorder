package be.timsys.gaming.acc.recorder.game.acc;


import be.timsys.gaming.acc.recorder.game.acc.StructWriter;

import java.io.IOException;

class FocusRequest {
    private int connectionId;
    private short carIndex;

    FocusRequest(int connectionId, short carIndex) {
        this.connectionId = connectionId;
        this.carIndex = carIndex;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 50);
        structWriter.writeInt(connectionId);
        structWriter.writeByte((byte) 1);
        structWriter.writeShort(carIndex);
        structWriter.writeByte((byte) 0);

        return structWriter.toByteArray();
    }
}
