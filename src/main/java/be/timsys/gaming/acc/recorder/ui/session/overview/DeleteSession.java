package be.timsys.gaming.acc.recorder.ui.session.overview;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class DeleteSession {
    private final String sessionId;
}
