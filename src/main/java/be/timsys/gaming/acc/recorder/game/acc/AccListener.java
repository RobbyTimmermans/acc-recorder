package be.timsys.gaming.acc.recorder.game.acc;

import be.timsys.gaming.acc.recorder.game.events.GameError;
import be.timsys.gaming.acc.recorder.game.events.GameNotResponding;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.PortUnreachableException;
import java.net.SocketTimeoutException;

@Service
@RequiredArgsConstructor
public class AccListener {

    private final AccMessageFactory accMessageFactory;
    private final ApplicationEventPublisher publisher;
    private final Logger logger;

    private AccClient accClient;
    private SocketReader socketReader;
    private Thread thread;

    void startListening(InetAddress ip, int port, String userName, String pwd, int timeout) throws Exception {
        accClient = null;
        thread = null;

        accClient = new AccClient(logger, userName, pwd);
        accClient.init(ip, port, timeout);

        socketReader = new SocketReader();

        thread = new Thread(socketReader, "SocketReader");
        thread.start();

        accClient.requestConnection();
    }

    void stop(Connection connection) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.shutdown(connection);
            accClient = null;
            thread.stop();
        }
    }

    void requestEntryList(Connection connection) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestEntryList(connection);
        }
    }

    private class SocketReader implements Runnable {
        @Override
        public void run() {
            if (accClient != null) {
                try {
                    byte[] read = accClient.read();
                    while (read != null) {
                        processMessage(read);
                        read = accClient.read();
                    }
                } catch (SocketTimeoutException | PortUnreachableException setex) {
                    accClient.close();
                    publisher.publishEvent(new GameNotResponding(setex.getMessage(), setex));
                } catch (Exception ex) {
                    logger.error("Error occurred", ex);
                    publisher.publishEvent(new GameError(ex));
                }
            }
        }

        private void processMessage(byte[] read) throws IOException {
            AccMessage accMessage = accMessageFactory.create(read);

            if (accMessage instanceof Connection) {
                accClient.requestEntryList((Connection) accMessage);
                accClient.requestTrackData((Connection) accMessage);
            }

            if (accMessage != null) {
                publisher.publishEvent(accMessage);
            }
        }
    }
}
