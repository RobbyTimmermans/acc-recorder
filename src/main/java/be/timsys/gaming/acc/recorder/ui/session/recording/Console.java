package be.timsys.gaming.acc.recorder.ui.session.recording;

import be.timsys.gaming.acc.recorder.game.events.GameError;
import be.timsys.gaming.acc.recorder.game.events.StatusChanged;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Comparator;

import static java.lang.String.format;

@Component
public class Console extends VBox {

    private ListView<Line> consoleList;

    @PostConstruct
    public void init() {
        setPadding(new Insets(5, 10, 5, 10));
        Label label = new Label("Console");
        consoleList = new ListView<>();
        consoleList.setCellFactory(new Callback<>() {
            @Override
            public ListCell<Line> call(ListView<Line> param) {
                return new ListCell<>() {
                    @Override
                    protected void updateItem(Line item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            var gridPane = new GridPane();
                            gridPane.add(new Label(item.getType().name()), 0, 0);
                            gridPane.add(new Label(String.format("%tT", item.getTime())), 1, 0);
                            gridPane.add(new Label(item.getMessageToShow()), 2, 0);
                            gridPane.setHgap(10);
                            gridPane.getColumnConstraints().add(0, new ColumnConstraints(50));
                            setGraphic(gridPane);
                        }
                    }
                };
            }
        });
        getChildren().add(label);
        getChildren().add(consoleList);
    }

    @EventListener
    public void on(StatusChanged statusChanged) {
        addLine(new Line(statusChanged.getTime(), statusChanged.getStatus(), MessageType.GAME));
    }

    @EventListener
    public void on(GameError gameError) {
        var exception = gameError.getException();
        addLine(new Line(gameError.getTime(), format("%s - %s - %s", exception.getClass(), exception.getMessage(), exception.getCause()), MessageType.SYSTEM));
    }

    private void addLine(Line line) {
        Platform.runLater(() -> {
            var latestLine = consoleList.getItems().size() > 0 ? consoleList.getItems().get(0) : null;
            if (latestLine != null && latestLine.getMessage().equals(line.getMessage()) && latestLine.getType() == line.getType()) {
                latestLine.setTimesReceivedSequential(latestLine.getTimesReceivedSequential() + 1);
                consoleList.getItems().set(0, latestLine);
            } else {
                consoleList.getItems().add(line);
                consoleList.getItems().sort(Comparator.comparing(Line::getTime).reversed());
            }
        });
    }

    void clear() {
        consoleList.getItems().clear();
    }

    @Getter
    @Setter
    private static class Line {
        private final LocalDateTime time;
        private String message;
        private final MessageType type;
        private int timesReceivedSequential = 0;

        private Line(LocalDateTime time, String message, MessageType type) {
            this.time = time;
            this.message = message;
            this.type = type;
        }

        String getMessageToShow() {
            return timesReceivedSequential > 0 ?
                    String.format("%s (%s)", message, timesReceivedSequential)
                    : message;
        }
    }

    private enum MessageType {
        GAME, SYSTEM
    }
}
