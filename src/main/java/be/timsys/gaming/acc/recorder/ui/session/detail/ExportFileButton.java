package be.timsys.gaming.acc.recorder.ui.session.detail;

import javafx.application.HostServices;
import javafx.scene.control.Button;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Scope("prototype")
@RequiredArgsConstructor
public class ExportFileButton extends Button {

    private final HostServices hostServices;

    public void setAction(ExportAction action) {
        this.setOnAction(event -> show(action.execute()));
    }

    private void show(File pdf) {
        hostServices.showDocument(pdf.toURI().toString());
    }
}
