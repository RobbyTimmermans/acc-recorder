package be.timsys.gaming.acc.recorder.analytics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
class GuiAction {

    private final String action;
}
