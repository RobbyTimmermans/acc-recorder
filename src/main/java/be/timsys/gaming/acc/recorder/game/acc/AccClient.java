package be.timsys.gaming.acc.recorder.game.acc;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

@RequiredArgsConstructor
public class AccClient {

    private final Logger logger;
    private final String userName;
    private final String pwd;

    private DatagramSocket clientSocket;

    void init(InetAddress ip, int port, int timeout) throws SocketException {
        logger.info("Connecting to {}", ip);
        clientSocket = new DatagramSocket(9998);
        clientSocket.setSoTimeout(timeout);
        clientSocket.connect(ip, port);
    }

    void requestConnection() throws Exception {
        logger.debug("Requesting connection");
        byte[] request = new ConnectionRequest(4, "ACCRECORDER", userName, pwd, 700).getBytes();
        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void requestTrackData(Connection connection) throws IOException {
        logger.debug("Requesting trackdata");
        byte[] request = new TrackDataRequest(connection.getConnectionId()).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void requestEntryList(Connection connection) throws IOException {
        logger.debug("Requesting entrylist");
        byte[] request = new EntryListRequest(connection.getConnectionId()).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void shutdown(Connection connection) throws IOException {
        logger.debug("Shutting down");
        if (connection != null) {
            byte[] request = new ShutdownRequest(connection.getConnectionId()).getBytes();
            clientSocket.send(new DatagramPacket(request, request.length));
        }
        close();
    }

    void close() {
        logger.info("Closing connection");
        if (clientSocket != null) {
            clientSocket.close();
        }
        clientSocket = null;
    }

    byte[] read() throws IOException {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, 1024);
        clientSocket.receive(receivePacket);
        return receiveData;
    }

    boolean isConnected() {
        return clientSocket != null;
    }
}
