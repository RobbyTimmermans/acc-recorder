package be.timsys.gaming.acc.recorder.ui.session.recording;

import be.timsys.gaming.acc.recorder.game.SessionInfo;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
public class WeatherInfoBar extends FlowPane {

    private Label timeOfDay;
    private Label temp;
    private Label weatherInfo;

    @PostConstruct
    public void init() {
        timeOfDay = new Label("-");
        temp = new Label("-");
        temp.setStyle("-fx-font-size: 80%");
        weatherInfo = new Label("-");
        weatherInfo.setStyle("-fx-font-size: 80%");

        setPadding(new Insets(5, 10, 5, 10));
        setHgap(10);

        this.getChildren().add(timeOfDay);
        this.getChildren().add(temp);
        this.getChildren().add(weatherInfo);
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        Platform.runLater(() -> {
            if (sessionInfo.getTimeOfDay() != null) {
                timeOfDay.setText(sessionInfo.getTimeOfDay().getFormattedTimeHM());
            }
            if (sessionInfo.getWeather() != null) {
                temp.setText(sessionInfo.getWeather().getTempDescription());
                weatherInfo.setText(sessionInfo.getWeather().getWeatherDescription());
            }
        });
    }
}
