package be.timsys.gaming.acc.recorder.ui.session;

import be.timsys.gaming.acc.recorder.game.SessionInfo;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
public class SessionInfoBar extends GridPane {

    private Label trackName;
    private Label sessionName;
    private Label sessionPhase;

    @PostConstruct
    public void init() {
        trackName = new Label("-");
        sessionName = new Label("-");
        sessionPhase = new Label("-");
        setStyle("-fx-background-color: lightgrey;");

        setPadding(new Insets(5, 10, 5, 10));

        setHgrow(trackName, Priority.ALWAYS);
        setHgrow(sessionName, Priority.ALWAYS);
        setHgrow(sessionPhase, Priority.ALWAYS);
        setHalignment(trackName, HPos.LEFT);
        setHalignment(sessionName, HPos.CENTER);
        setHalignment(sessionPhase, HPos.RIGHT);

        add(trackName, 0, 1);
        add(sessionName, 1, 1);
        add(sessionPhase, 2, 1);
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        Platform.runLater(() -> {
            trackName.setText(sessionInfo.getTrackName());
            sessionName.setText(sessionInfo.getSessionType() + " " + (sessionInfo.getSessionIndex() != null ? sessionInfo.getSessionIndex() : ""));
            sessionPhase.setText(sessionInfo.getSessionPhase());
        });
    }
}
