package be.timsys.gaming.acc.recorder.game.acc;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

@Getter
@ToString
class Driver {

    private short driverIndex;
    private String firstName;
    private String lastName;
    private String shortName;
    private byte category;
    private short nationality;

    Driver(short driverIndex, LittleEndianDataInputStream lil) throws IOException {
        this.driverIndex = driverIndex;
        this.firstName = AccMessage.readString(lil);
        this.lastName = AccMessage.readString(lil);
        this.shortName = AccMessage.readString(lil);
        this.category = lil.readByte();
        this.nationality = lil.readShort();
    }

    public String getDriverFullRepresentation() {
        return String.format("%s %s (%s - %s)", firstName, lastName, shortName, Nationality.fromId(nationality).getLabel());
    }

    public String getDriverFullName() {
        return String.format("%s %s", firstName, lastName);
    }
}
