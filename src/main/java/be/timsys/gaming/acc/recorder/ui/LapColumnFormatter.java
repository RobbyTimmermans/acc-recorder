package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.Lap;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.util.Callback;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LapColumnFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    @Override
    public TableCell<S, T> call(TableColumn<S, T> arg0) {
        return new TableCell<>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    Lap lap = (Lap) item;
                    var label = new Label(lap.getFormattedTime());
                    if (lap.getSplits() != null) {
                        label.setTooltip(new Tooltip(LapColumnFormatter.this.getTooltip(lap)));
                    }
                    setGraphic(label);
                }
            }
        };
    }

    public String getTooltip(Lap lap) {
        int numberOfSplits = lap.getSplits().size();
        return IntStream.range(0, numberOfSplits).mapToObj(splits -> String.format("S sp%s: %s", splits + 1, lap.getFormattedSplits(splits))).collect(Collectors.joining(","));
    }
}
