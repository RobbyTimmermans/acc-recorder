package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.Driver;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.util.Callback;

public class DriverColumnFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    private Type type;

    public DriverColumnFormatter() {
        this(Type.SHORTNAME);
    }

    public DriverColumnFormatter(Type type) {
        this.type = type;
    }

    @Override
    public TableCell<S, T> call(TableColumn<S, T> arg0) {
        return new TableCell<>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    var driver = (Driver) item;
                    var label = new Label(getDriverName(driver));
                    label.setTooltip(new Tooltip(String.format("%s (%s - %s)", driver.getDriverFullName(), driver.getNationality(), driver.getCup())));
                    setGraphic(label);
                }
            }
        };
    }

    private String getDriverName(Driver driver) {
        if (type == Type.FULLNAME) {
            return driver.getDriverFullName();
        } else {
            return driver.getShortName();
        }
    }

    public enum Type {
        FULLNAME, SHORTNAME;
    }
}
