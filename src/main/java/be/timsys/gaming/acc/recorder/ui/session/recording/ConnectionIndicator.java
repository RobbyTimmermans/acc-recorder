package be.timsys.gaming.acc.recorder.ui.session.recording;

import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ConnectionIndicator extends Rectangle {

    private int rotation = 0;
    private final Timer timer;

    public ConnectionIndicator() {
        super(15, 15, Color.RED);
        timer = new Timer();
    }

    public void setConnected() {
        timer.stop();
        setFill(Color.GREEN);
        setRotate(0);
    }

    public void setDisconnected() {
        timer.stop();
        setFill(Color.DARKGREY);
        setRotate(0);
    }

    public void setConnecting() {
        timer.start();
        setFill(Color.ORANGE);
    }

    private class Timer extends AnimationTimer {
        private long lastUpdate = 9000000 ;
        @Override
        public void handle(long now) {
            if (now - lastUpdate >= 9000000) {
                rotation = rotation + 5;
                if (rotation > 360) {
                    rotation = 0;
                }
                setRotate(rotation);
                lastUpdate = now;
            }
        }
    }
}
