package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.standings.SessionStandings;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SessionStandingsUpdated {

    private final String sessionId;

    private final SessionStandings sessionStandings;

}
