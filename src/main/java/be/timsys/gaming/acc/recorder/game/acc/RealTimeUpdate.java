package be.timsys.gaming.acc.recorder.game.acc;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

@Getter
@ToString
class RealTimeUpdate extends AccMessage {

    private short eventIndex;
    private short sessionIndex;
    private short sessionType;
    private short sessionPhase;
    private Float sessionTime;
    private Float sessionEndTime;
    private int focusedCarIndex;
    private String activeCameraSet;
    private String activeCamera;
    private String currentHudPage;

    private boolean isReplayPlaying;
    private String replaySessionTime;
    private String replayRemainingTime;
    private int replayFocusedCar;

    private Double timeOfDay;
    private short ambientTemp;
    private short trackTemp;
    private short clouds;
    private short rainLevel;
    private short wetness;

    private Lap bestSessionLap;

    RealTimeUpdate(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.eventIndex =  lil.readShort();
        this.sessionIndex = lil.readShort();
        this.sessionType = lil.readByte();
        this.sessionPhase = lil.readByte();
        this.sessionTime = lil.readFloat();
        this.sessionEndTime = lil.readFloat();
        this.focusedCarIndex = lil.readInt();
        this.activeCameraSet = readString(lil);
        this.activeCamera = readString(lil);
        this.currentHudPage = readString(lil);
        this.isReplayPlaying = lil.readBoolean();
        this.timeOfDay = Double.valueOf(lil.readFloat());
        this.ambientTemp = lil.readByte();
        this.trackTemp = lil.readByte();
        this.clouds = lil.readByte();
        this.rainLevel= lil.readByte();
        this.wetness = lil.readByte();
//        this.bestSessionLap = new Lap(lil);
    }
}
