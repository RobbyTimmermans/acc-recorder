package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.Split;
import be.timsys.gaming.acc.recorder.ui.session.detail.Indicator;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.util.Callback;

public class TableUtils {

    public static <S> void addColumn(TableView<S> tableView, String label, String property, double widthPercent) {
        addColumn(tableView, label, property, widthPercent, null);
    }

    public static <S> void addDateColumn(TableView<S> tableView, String label, String property, double widthPercent) {
        addColumn(tableView, label, property, widthPercent, new DateTimeColumnFormatter<>());
    }

    public static <S> void addLapColumn(TableView<S> tableView, String label, String property, double widthPercent) {
        addColumn(tableView, label, property, widthPercent, new LapColumnFormatter<>());
    }

//    public static <S> void addColumn(TableView<S> tableView, String label, String property, double widthPercent, Callback<TableColumn<S, String>, TableCell<S, String>> cellFactory) {
//        var column = new TableColumn<S, String>(label);
//        column.prefWidthProperty().bind(tableView.widthProperty().multiply(widthPercent / 100));
//
//        column.setCellValueFactory(new PropertyValueFactory<>(property));
//        if (cellFactory != null) {
//            column.setCellFactory(cellFactory);
//        }
//        tableView.getColumns().add(column);
//    }

    public static <S> void addColumn(TableView<S> tableView, String label, String property, double widthPercent, Callback<TableColumn<S, Object>, TableCell<S, Object>> cellFactory) {
        var column = new TableColumn<S, Object>(label);
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(widthPercent / 100));

        column.setCellValueFactory(new PropertyValueFactory<>(property));
        if (cellFactory != null) {
            column.setCellFactory(cellFactory);
        }
        tableView.getColumns().add(column);
    }

    public static <S> void addSectorColumn(TableView<S> tableView, String label, Integer splitsIndex, double widthPercent) {
        TableColumn<Lap, Split> column = new TableColumn<>(label);
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(widthPercent / 100));
        column.setCellValueFactory(p -> p.getValue().getSplits(splitsIndex).map(SimpleObjectProperty::new).orElse(null));

        column.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Lap, Split> call(TableColumn<Lap, Split> param) {
                return new TableCell<>() {
                    @Override
                    protected void updateItem(Split item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            var pane = new FlowPane();
                            pane.setHgap(5);
                            pane.setPrefHeight(10);
                            pane.getChildren().add(new Label(item.getFormattedSplit()));
                            if (item.isFastest()) {
                                pane.getChildren().add(new Indicator("mediumpurple", "Fastest sector"));
                            }
                            setGraphic(pane);
                        }
                    }
                };
            }
        });

        tableView.getColumns().add((TableColumn<S, ?>) column);
    }
}
