package be.timsys.gaming.acc.recorder.game.standings;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.SessionEvent;
import be.timsys.gaming.acc.recorder.game.commands.DisconnectedEvent;
import be.timsys.gaming.acc.recorder.game.events.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StandingService {

    private final ApplicationEventPublisher publisher;
    private final Logger logger;

    private SessionStandings sessionStandings = new SessionStandings();

    @EventListener
    public void on(NewSessionEntry newSessionEntry) {
        logger.debug("Adding new sessionEntry {}", newSessionEntry.getSessionEntry().getDriver());
        sessionStandings.addEntry(newSessionEntry.getSessionEntry());

        publishStandingsUpdated(newSessionEntry.getSessionId());
    }

    @EventListener
    public void on(PositionUpdated positionUpdated) {
        sessionStandings.updatePosition(positionUpdated.getCarIndex(), positionUpdated.getNewPosition());
        publishStandingsUpdated(positionUpdated.getSessionId());

        if (positionUpdated.getOldPosition() > 0 && positionUpdated.gainedPosition()) {
            var positionSwitchMessage = sessionStandings.getSessionEntries().stream().filter(e -> e.getId() == positionUpdated.getCarIndex())
                    .findFirst()
                    .map(SessionEntry::getDriver)
                    .map(d -> String.format("%s from P%s to P%s", d, positionUpdated.getOldPosition(), positionUpdated.getNewPosition())).orElse(null);
            publisher.publishEvent(new StatusChanged(positionSwitchMessage));
        }
    }

    @EventListener
    public void on(LapCompleted lapCompleted) {
        sessionStandings.addLap(lapCompleted.getCarId(), lapCompleted.getLap());
        publishStandingsUpdated(lapCompleted.getSessionId());
    }

    @EventListener
    public void on(DriverChanged driverChanged) {
        sessionStandings.find(driverChanged.getCarIndex()).ifPresent(car -> {
            sessionStandings.driverChange(driverChanged.getCarIndex(), driverChanged.getNewDriver());
            publishStandingsUpdated(driverChanged.getSessionId());
            publisher.publishEvent(new StatusChanged(String.format("Driver change for #%s: %s out, %s in", car.getNumber(), driverChanged.getOldDriver().getDriverFullName(), driverChanged.getNewDriver().getDriverFullName())));
        });
    }

    @EventListener
    public void on(BroadCastEvent event) {
        sessionStandings.find(event.getCarIndex()).ifPresent(car -> {
            sessionStandings.addEvent(car.getId(), SessionEvent.builder()
                    .description(event.getDescription())
                    .timeOfDay(event.getTimeOfDay())
                    .realTime(event.getRealTime())
                    .type(event.getType())
                    .driver(car.getCurrentDriver())
                    .car(car.getCar())
                    .number(car.getNumber())
                    .lapNumber(car.getNumberOfLaps())
                    .build());
            publisher.publishEvent(new StatusChanged(String.format("%s reported for #%s - %s in car %s", event.getType().getLabel(), car.getNumber(), car.getCurrentDriver().getDriverFullName(), car.getCar())));
        });
    }

    @EventListener
    public void on(NewSessionStarted newSessionStarted) {
        sessionStandings = new SessionStandings();
    }

    @EventListener
    public void on(DisconnectedEvent disconnectedEvent) {
        sessionStandings = new SessionStandings();
    }

    private void publishStandingsUpdated(String sessionId) {
        publisher.publishEvent(new SessionStandingsUpdated(sessionId, sessionStandings));
    }

}
