package be.timsys.gaming.acc.recorder.ui.session.overview;

import be.timsys.gaming.acc.recorder.config.ConfigDialog;
import be.timsys.gaming.acc.recorder.config.ConfigService;
import be.timsys.gaming.acc.recorder.session.SessionService;
import be.timsys.gaming.acc.recorder.ui.session.TopBar;
import be.timsys.gaming.acc.recorder.ui.session.recording.RecordingSession;
import be.timsys.javafx.scene.ShowScreenCommand;
import be.timsys.javafx.scene.UiScreen;
import javafx.application.HostServices;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.Arrays;

import static java.util.Collections.singletonList;

@Component
@RequiredArgsConstructor
public class SessionOverview implements UiScreen<String> {

    private final SessionsTable sessionTable;
    private final SessionService sessionService;
    private final ApplicationEventPublisher eventPublisher;
    private final HostServices hostServices;
    private final ConfigDialog configDialog;
    private final ConfigService configService;

    private Scene scene;

    @Override
    public Scene getContent() {
        return scene;
    }

    @Override
    public void init() {
        BorderPane layout = new BorderPane();

        layout.setTop(createTopBar());
        layout.setCenter(sessionTable);
        this.scene = new Scene(layout);
    }

    @Override
    public void onShow(String argument) {
        refreshData();
    }

    private void refreshData() {
        sessionTable.setData(sessionService.findAll());
    }

    @Override
    public boolean isStartPage() {
        return true;
    }

    private Node createTopBar() {
        var startRecording = new Button("Start Recording...");
        startRecording.setOnAction(event -> eventPublisher.publishEvent(new ShowScreenCommand<>(RecordingSession.class.getName())));

        ButtonType loginButtonType = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("About");
        var pane = new FlowPane();
        pane.getChildren().add(new Label("ACC Recorder V0.0.8-SNAPSHOT\nTimsys 2020\nMore info here:"));
        var link = new Hyperlink("https://bitbucket.org/RobbyTimmermans/acc-recorder");
        link.setOnAction(e -> hostServices.showDocument("https://bitbucket.org/RobbyTimmermans/acc-recorder"));
        pane.getChildren().add(link);
        dialog.getDialogPane().setContent(pane);

        dialog.getDialogPane().getButtonTypes().add(loginButtonType);
        var about = new Button("About");
        about.setOnAction(event -> dialog.show());

        var config = new Button("Settings");
        config.setOnAction(event -> {
            configDialog.showAndWait().ifPresent(configService::save);
        });
        return new TopBar(Arrays.asList(about, config), "Sessions", singletonList(startRecording));
    }
}
