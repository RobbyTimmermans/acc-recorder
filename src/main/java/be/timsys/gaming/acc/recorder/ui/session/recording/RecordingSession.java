package be.timsys.gaming.acc.recorder.ui.session.recording;

import be.timsys.gaming.acc.recorder.game.commands.DisconnectedEvent;
import be.timsys.gaming.acc.recorder.game.events.ConnectedEvent;
import be.timsys.gaming.acc.recorder.game.events.StatusChanged;
import be.timsys.gaming.acc.recorder.ui.GameService;
import be.timsys.gaming.acc.recorder.ui.StatusBar;
import be.timsys.gaming.acc.recorder.ui.session.TopBar;
import be.timsys.gaming.acc.recorder.ui.session.overview.SessionOverview;
import be.timsys.javafx.scene.ShowScreenCommand;
import be.timsys.javafx.scene.UiScreen;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@Component
@RequiredArgsConstructor
public class RecordingSession implements UiScreen<Object> {

    private final Console console;
    private final CurrentSession currentSession;
    private final StatusBar statusBar;
    private final GameService gameService;
    private final ApplicationEventPublisher eventPublisher;

    private Scene scene;
    private Button startRecording;
    private Button stopRecording;
    private Button allSessions;
    private ConnectionIndicator connectionIndicator;

    @Override
    public Scene getContent() {
        return scene;
    }

    @Override
    public void init() {
        BorderPane layout = new BorderPane();
        console.setPrefHeight(200);

        layout.setTop(createTopBar());
        var center = new BorderPane();
        center.setCenter(currentSession);
        center.setBottom(console);
        layout.setCenter(center);
        layout.setBottom(statusBar);

        this.scene = new Scene(layout);
    }

    @Override
    public void onShow(Object argument) {
        currentSession.clear();
        console.clear();

        startRecording();
    }

    @EventListener
    public void on(DisconnectedEvent disconnectedEvent) {
        startRecording.setDisable(false);
        stopRecording.setDisable(true);
        allSessions.setDisable(false);
        connectionIndicator.setDisconnected();
    }

    @EventListener
    public void on(ConnectedEvent connectedEvent) {
        connectionIndicator.setConnected();
        eventPublisher.publishEvent(new StatusChanged(String.format("Connected to %s:%s", connectedEvent.getIp(), connectedEvent.getPort())));
    }

    private Node createTopBar() {
        startRecording = new Button("Start Recording...");
        startRecording.setOnAction(event -> {
            startRecording();
        });

        stopRecording = new Button("Stop Recording...");
        stopRecording.setOnAction(event -> stopRecording());
        stopRecording.setDisable(true);

        allSessions = new Button("Back");
        allSessions.setOnAction(event -> eventPublisher.publishEvent(new ShowScreenCommand<>(SessionOverview.class.getName())));

        connectionIndicator = new ConnectionIndicator();
        return new TopBar(singletonList(allSessions), "Record session", asList(connectionIndicator, startRecording, stopRecording));
    }

    private void stopRecording() {
        gameService.disconnect();
    }

    private void startRecording() {
        startRecording.setDisable(true);
        stopRecording.setDisable(false);
        allSessions.setDisable(true);
        gameService.connect();
        connectionIndicator.setConnecting();
    }
}
