package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.config.ConfigService;
import be.timsys.gaming.acc.recorder.game.acc.Acc;
import be.timsys.gaming.acc.recorder.game.events.GameNotResponding;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.net.PortUnreachableException;

@Service
@RequiredArgsConstructor
public class GameService {

    private final Acc acc;
    private final ConfigService configService;
    private final ApplicationEventPublisher eventPublisher;
    private final Logger logger;

    private boolean retryOnFailure;

    @SneakyThrows
    public void connect() {
        logger.debug("Connecting to ACC");
        retryOnFailure = true;
        var config = configService.getConfig();
        acc.connect(config.getServer(), config.getPort(), config.getUser(), config.getPwd(), config.getReadTimeoutMs());
    }

    @SneakyThrows
    @EventListener
    public void on(GameNotResponding gameNotResponding) {
        logger.debug("Game not responding {} - {}", gameNotResponding.getReason(), gameNotResponding.getCause());

        if (retryOnFailure) {
            logger.debug("Retrying ...");
            waitIfGameNotReachable(gameNotResponding);
            connect();
        }
    }

    @SneakyThrows
    public void disconnect() {
        retryOnFailure = false;
        acc.disconnect();
    }

    private void waitIfGameNotReachable(GameNotResponding gameNotResponding) throws InterruptedException {
        if (gameNotResponding.getCause() != null && gameNotResponding.getCause() instanceof PortUnreachableException) {
            logger.debug("Wait for game to become reachable");
            Thread.sleep(configService.getConfig().getRetryDelayOnGameNotReachableMs());
        }
    }
}
