package be.timsys.gaming.acc.recorder.game.acc;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Weather {

    private int rain;
    private int wetness;
    private int cloudCover;
    private int ambientTemp;
    private int trackTemp;

    @JsonIgnore
    public String getWeatherDescription() {
        var rainDescription = "dry";
        if (rain > 0 && rain <= 2) {
            rainDescription = "light rain";
        } else if (rain > 2 && rain <= 5) {
            rainDescription = "rain";
        } else if (rain > 5 && rain <= 7) {
            rainDescription = "heavy rain";
        } else if (rain > 7) {
            rainDescription = "very heavy rain";
        }

        return String.format("%s (%s) - %s%% clouds - %s%% wetness", rainDescription, rain, cloudCover * 10, wetness * 10);
    }

    public String getTempDescription() {
        return String.format("%sC - Track %sC", ambientTemp, trackTemp);
    }
}
