package be.timsys.gaming.acc.recorder.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Base64;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class AccRecorderApi {

    private final ObjectMapper objectMapper;
    private final Properties apiProperties;

    @SneakyThrows
    @PostConstruct
    void init() {
        apiProperties.load(this.getClass().getResourceAsStream("/api.properties"));
    }

    public <T, R> T post(String path, R request, Function<String, T> getResult) throws JsonProcessingException, ExecutionException, InterruptedException {
        var client = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(20))
                .build();
        var url = format(String.format("%s/%%s", apiProperties.getProperty("api.base-url")), path);
        var post = HttpRequest.newBuilder(URI.create(url))
                .header("Content-type", "application/json")
                .header("X-api-key", getApiKey())
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(request)))
                .build();

        return client.sendAsync(post, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(getResult)
                .get();
    }

    private String getApiKey() {
        var encoded = apiProperties.getProperty("api.key");
        var decoded = Base64.getDecoder().decode(Base64.getDecoder().decode(encoded));
        return new String(decoded, StandardCharsets.UTF_8);
    }

}
