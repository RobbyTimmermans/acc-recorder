package be.timsys.gaming.acc.recorder.game.acc;

import be.timsys.gaming.acc.recorder.game.Driver;
import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.SessionInfo;
import be.timsys.gaming.acc.recorder.game.commands.DisconnectedEvent;
import be.timsys.gaming.acc.recorder.game.events.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

import static be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType.NONE;
import static be.timsys.gaming.acc.recorder.game.acc.Nationality.Any;
import static be.timsys.gaming.acc.recorder.game.acc.Nationality.fromId;
import static java.time.OffsetDateTime.now;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class Acc {

    private final AccListener accListener;
    private final ApplicationEventPublisher publisher;
    private final Logger logger;

    private Connection connection;
    private EntryList entryList;
    private Map<Short, EntryListCar> carMap = new HashMap<>();
    private Map<Short, EntryListCarStats> carStatsMap = new HashMap<>();

    private TrackData trackData;
    private short currentSession = -1;
    private short currentSessionPhase = -1;
    private short currentEventIndex = -1;
    private short currentSessionIndex = -1;
    private String currentSessionId = null;
    private TimeOfDay currentTimeOfDay;
    private Weather currentWeather;

    private String ip;
    private int port;

    public void connect(String ip, int port, String user, String pwd, int timeout) throws Exception {
        this.ip = ip;
        this.port = port;
        publisher.publishEvent(new StatusChanged(String.format("Connecting to %s:%s", ip, port)));
        accListener.startListening(InetAddress.getByName(ip), port, user, pwd, timeout);
    }

    public void disconnect() throws IOException {
        logger.debug("Disconnecting");
        publisher.publishEvent(new DisconnectedEvent());
        publisher.publishEvent(new StatusChanged("Disconnected"));

        accListener.stop(connection);
        logger.debug("Disconnected");
        clearData();
    }

    void requestEntryList() throws IOException {
        accListener.requestEntryList(connection);
    }

    @EventListener
    public void onConnection(Connection connection) {
        this.connection = connection;
        publisher.publishEvent(new ConnectedEvent(ip, port));
    }

    @EventListener
    public void onEntryList(EntryList entryList) {
        this.entryList = entryList;
        if (entryList != null) {
            this.carMap = new HashMap<>();
            entryList.getCarList().forEach(c -> carMap.put(c.getCarIndex(), c));
        }
    }

    @EventListener
    public void onEntryListCar(EntryListCar entryListCar) {
        logger.trace("onEntryListCar {}", entryList);
        carMap.put(entryListCar.getCarIndex(), entryListCar);
        publishNewSessionEntry(entryListCar);
    }

    @EventListener
    public void onTrackData(TrackData trackData) {
        this.trackData = trackData;
        publishSessionInfoChanged();
    }

    @EventListener
    public void onBroadCastMessage(BroadCastMessage broadCastMessage) {
        if (entryList != null) {
            var carId = (short) broadCastMessage.getCarId();
            if (carStatsMap.containsKey(carId)) {
                var lastMessage = carStatsMap.get(carId).getLastMessage().orElse(null);
                if (lastMessage == null || lastMessage != broadCastMessage) {
                    var type = BroadCastMessageType.fromId(broadCastMessage.getType()).orElse(NONE);
                    switch (type) {
                        case BEST_PERSONAL_LAP, BEST_SESSION_LAP, ACCIDENT, PENALTY_COMM_MSG -> publisher.publishEvent(BroadCastEvent.builder()
                                .sessionId(currentSessionId)
                                .carIndex(carId)
                                .description(broadCastMessage.getMessage())
                                .type(type)
                                .timeOfDay(currentTimeOfDay)
                                .realTime(now())
                                .build());
                    }
                }
            }
            if (carStatsMap != null && carStatsMap.containsKey(carId)) {
                carStatsMap.get(carId).setLastMessage(broadCastMessage);
            }
        }
    }

    @EventListener
    public void onRealtimeCarUpdate(RealTimeCarUpdate update) throws IOException {
        logger.trace(update.toString());
        if (entryList != null) {
            if (!carMap.containsKey(update.getCarIndex())) {
                logger.debug("Update for unknown car, requesting entry list");
                requestEntryList();
            } else {

                var lastLap = update.getLastLap();
                if (carMap.get(update.getCarIndex()) != null) {
                    short oldPosition = ofNullable(carStatsMap.get(update.getCarIndex())).map(EntryListCarStats::getSessionPosition).orElse((short) -1);
                    short newPosition = update.getSessionPosition();
                    if (oldPosition != newPosition || oldPosition == 0) {
                        publisher.publishEvent(new PositionUpdated(currentSessionId, update.getCarIndex(), oldPosition, newPosition));
                    }

                    short oldIndex = ofNullable(carStatsMap.get(update.getCarIndex())).map(EntryListCarStats::getDriverIndex).orElse((short) -1);
                    var newIndex = update.getDriverIndex();
                    if (oldIndex != -1 && oldIndex != newIndex) {
                        var drivers = carMap.get(update.getCarIndex()).getDrivers();
                        if (drivers != null) {
                            publisher.publishEvent(new DriverChanged(currentSessionId, update.getCarIndex(), toDriver(drivers.get(oldIndex)), toDriver(drivers.get(newIndex))));
                        }
                    }

                    var carLastLapTime = ofNullable(carStatsMap.get(update.getCarIndex())).map(EntryListCarStats::getCarLastLapTime).orElse(-1);
                    if (!carLastLapTime.equals(lastLap.getLaptimeInMs())) {
                        publisher.publishEvent(new LapCompleted(currentSessionId, update.getCarIndex(), Lap.builder()
                                .lapNumber((int) update.getLaps())
                                .laptimeInMs(lastLap.getLaptimeInMs())
                                .splits(lastLap.toSplits())
                                .isValid(!lastLap.isInvalid())
                                .isInLap(lastLap.isInlap())
                                .isOutLap(lastLap.isOutlap())
                                .positionOnLapCompleted(update.getSessionPosition())
                                .gameTimeOfDay(currentTimeOfDay)
                                .realRecordedTime(now())
                                .weather(currentWeather)
                                .driver(toDriver(carMap.get(update.getCarIndex()).getDriver(update.getDriverIndex())))
                                .build())
                        );
                    }
                }

                var stats = carStatsMap.get(update.getCarIndex());
                if (stats == null) {
                    stats = new EntryListCarStats();
                }
                stats.setSessionPosition(update.getSessionPosition());
                stats.setCarLocation(update.getCarLocation());
                stats.setCarLastLapTime(lastLap.getLaptimeInMs());

                stats.setDriverIndex(update.getDriverIndex());
                carStatsMap.put(update.getCarIndex(), stats);
            }
        }
    }

    @EventListener
    public void onRealTimeUpdate(RealTimeUpdate realTimeUpdate) {
        if (this.currentSessionIndex != realTimeUpdate.getSessionIndex()
                || this.currentSession != realTimeUpdate.getSessionType()
                || (realTimeUpdate.getSessionPhase() == SessionPhase.PRE_FORMATION.getId() && currentSessionPhase != realTimeUpdate.getSessionPhase())
        ) {
            this.currentSessionIndex = realTimeUpdate.getSessionIndex();
            publishNewSessionStarted(realTimeUpdate.getSessionType(), realTimeUpdate.getEventIndex(), realTimeUpdate.getSessionIndex());
        }

        if (this.currentSession != realTimeUpdate.getSessionType()
                || this.currentSessionPhase != realTimeUpdate.getSessionPhase()
                || this.currentEventIndex != realTimeUpdate.getEventIndex()
                || this.currentSessionIndex != realTimeUpdate.getSessionIndex()
        ) {
            this.currentSession = realTimeUpdate.getSessionType();
            this.currentSessionPhase = realTimeUpdate.getSessionPhase();
            this.currentEventIndex = realTimeUpdate.getEventIndex();
            this.currentSessionIndex = realTimeUpdate.getSessionIndex();
            publishSessionInfoChanged();
        }

        storeTime(realTimeUpdate);
        currentWeather = new Weather(realTimeUpdate.getRainLevel(), realTimeUpdate.getWetness(), realTimeUpdate.getClouds(), realTimeUpdate.getAmbientTemp(), realTimeUpdate.getTrackTemp());
    }

    private void storeTime(RealTimeUpdate realTimeUpdate) {
        var newTimeOfDay = new TimeOfDay(realTimeUpdate.getTimeOfDay());
        if (currentTimeOfDay == null || newTimeOfDay.getMinutes() != currentTimeOfDay.getMinutes()) {
            publishSessionInfoChanged();
        }
        this.currentTimeOfDay = newTimeOfDay;
    }

    private void publishSessionInfoChanged() {
        publisher.publishEvent(new SessionInfoChanged(SessionInfo.builder()
                .trackName(trackData != null ? trackData.getTrackName() : "")
                .trackId(trackData != null ? trackData.getTrackId() : -1)
                .sessionPhase(SessionPhase.fromId(currentSessionPhase).map(Objects::toString).orElse(""))
                .sessionType(SessionType.fromId(currentSession).map(Objects::toString).orElse(""))
                .event(String.valueOf(currentEventIndex))
                .sessionIndex(String.valueOf(currentSessionIndex))
                .sessionId(currentSessionId)
                .weather(currentWeather)
                .timeOfDay(currentTimeOfDay)
                .build()
        ));
    }

    private void publishNewSessionStarted(short sessionType, short eventIndex, short sessionIndex) {
        clearData();
        this.currentSessionId = UUID.randomUUID().toString();
        publisher.publishEvent(NewSessionStarted.builder()
                .sessionType(SessionType.fromId(sessionType).orElseThrow())
                .eventIndex(eventIndex)
                .sessionIndex(sessionIndex)
                .trackName(trackData != null ? trackData.getTrackName() : "")
                .trackId(trackData != null ? trackData.getTrackId() : -1)
                .id(currentSessionId)
                .build());
    }

    private void publishNewSessionEntry(EntryListCar entryListCar) {
        publisher.publishEvent(new NewSessionEntry(
                currentSessionId,
                SessionEntry.builder()
                        .id(entryListCar.getCarIndex())
                        .driver(entryListCar.getCurrentDriver().getDriverFullName())
                        .currentDriver(toDriver(entryListCar.getCurrentDriver()))
                        .drivers(entryListCar.getDrivers().stream().map(this::toDriver).collect(toList()))
                        .number(String.valueOf(entryListCar.getRaceNumber()))
                        .car(entryListCar.getCarModelType() != null ? entryListCar.getCarModelType().getDescription() : String.valueOf(entryListCar.getCarModelType()))
                        .carType(entryListCar.getCarModelType())
                        .teamName(entryListCar.getTeamName())
                        .nationality(entryListCar.getNationality() != Any.getId() ? fromId(entryListCar.getNationality()) : fromId(entryListCar.getCurrentDriver().getNationality()))
                        .build()));
    }

    private Driver toDriver(be.timsys.gaming.acc.recorder.game.acc.Driver driver) {
        if (driver != null) {
            return Driver.builder()
                    .firstName(driver.getFirstName())
                    .lastName(driver.getLastName())
                    .shortName(driver.getShortName())
                    .cup(Cup.fromId((short) driver.getCategory()))
                    .nationality(fromId(driver.getNationality()))
                    .driverFullName(driver.getDriverFullName())
                    .build();
        } else {
            return null;
        }
    }

    private Driver getCurrentDriver(short carId) {
        return carMap.containsKey(carId) ? toDriver(carMap.get(carId).getCurrentDriver()) : null;
    }

    private void clearData() {
        logger.debug("Clearing data");
        carMap.clear();
        carStatsMap.clear();
        currentSessionId = null;
        currentSession = -1;
        currentSessionPhase = -1;
        currentEventIndex = -1;
        currentSessionIndex = -1;
        currentTimeOfDay = null;
        currentWeather = null;
    }
}
