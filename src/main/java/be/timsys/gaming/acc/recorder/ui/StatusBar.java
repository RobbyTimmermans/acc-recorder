package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.events.StatusChanged;
import javafx.application.Platform;
import javafx.scene.control.Label;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StatusBar extends Label {

    @EventListener
    public void onStatusChanged(StatusChanged statusChanged) {
        Platform.runLater(() -> setText(statusChanged.getStatus()));
    }
}
