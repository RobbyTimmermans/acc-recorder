package be.timsys.gaming.acc.recorder.game.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Getter
@RequiredArgsConstructor
public class StatusChanged {
    private final LocalDateTime time = LocalDateTime.now();
    private final String status;
}
