package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.session.Session;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SessionEntryDetailParam {

    private final Session session;

    private final SessionEntry sessionEntry;

    @Override
    public String toString() {
        return session.getId() + " " + sessionEntry.getDriver();
    }
}
