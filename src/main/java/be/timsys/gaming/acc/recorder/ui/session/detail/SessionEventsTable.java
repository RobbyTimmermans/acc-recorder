package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.game.SessionEvent;
import be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.DriverColumnFormatter;
import be.timsys.gaming.acc.recorder.ui.EventTypeFormatter;
import be.timsys.gaming.acc.recorder.ui.TimeOfDayColumnFormatter;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType.*;
import static be.timsys.gaming.acc.recorder.ui.DriverColumnFormatter.Type.FULLNAME;
import static be.timsys.gaming.acc.recorder.ui.TableUtils.addColumn;

@Component
@Scope("prototype")
@RequiredArgsConstructor
public class SessionEventsTable extends BorderPane {

    private TableView<SessionEvent> tableView;
    private Session session;

    @PostConstruct
    public void init() {
        tableView = new TableView<>();

        addColumn(tableView, "Time", "timeOfDay", 8, new TimeOfDayColumnFormatter<>());
        addColumn(tableView, "L", "lapNumber", 3);
        addColumn(tableView, "Driver", "driver", 20, new DriverColumnFormatter<>(FULLNAME));
        addColumn(tableView, "Car", "car", 25);
        addColumn(tableView, "Event", "type", 15, new EventTypeFormatter<>());
        addColumn(tableView, "Description", "description", 20);


        this.setTop(creatFilter());
        this.setCenter(tableView);
    }

    private Node creatFilter() {
        var eventFilter = new ComboBox<EventFilter>();
        eventFilter.getItems().add(new EventFilter("All", ACCIDENT, BEST_PERSONAL_LAP, BEST_SESSION_LAP, PENALTY_COMM_MSG));
        eventFilter.getItems().add(new EventFilter("Incidents", ACCIDENT));
        eventFilter.getItems().add(new EventFilter("Penalty messages", PENALTY_COMM_MSG));
        eventFilter.getItems().add(new EventFilter("Best Personal laps", BEST_PERSONAL_LAP));
        eventFilter.getItems().add(new EventFilter("Best Session laps", BEST_SESSION_LAP));
        eventFilter.getSelectionModel().select(0);
        eventFilter.setCellFactory(new EventFilterListCellFactory<>());
        eventFilter.setConverter(new EventFilterStringConverter<>());
        eventFilter.setOnAction((event) -> {
            filter(eventFilter.getSelectionModel().getSelectedItem());
        });

        eventFilter.setMaxHeight(10);

        var filter = new HBox(eventFilter);
        filter.setMaxHeight(10);
        filter.setPadding(new Insets(10, 10, 10, 10));
        return filter;
    }

    private void filter(EventFilter filter) {
        setEvents(session.getSessionStandings().getSessionEvents(filter.getTypes()));
    }

    public void setData(Session session) {
        this.session = session;
        if (session.getSessionStandings() != null && session.getSessionStandings().getSessionEvents() != null) {
            setEvents(session.getSessionStandings().getSessionEvents(values()));
        }
    }

    private void setEvents(List<SessionEvent> events) {
        tableView.getItems().clear();
        if (events != null) {
            tableView.getItems().addAll(events);
        }
    }

    @Getter
    private static class EventFilter {
        private final String label;
        private final BroadCastMessageType[] types;

        private EventFilter(String label, BroadCastMessageType... types) {
            this.label = label;
            this.types = types;
        }
    }

    private static class EventFilterListCellFactory<P, R> implements Callback<ListView<P>, ListCell<R>> {

        @Override
        public ListCell<R> call(ListView<P> param) {
            return new ListCell<>() {
                @Override
                protected void updateItem(R item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        var label = new Label(((EventFilter) item).getLabel());
                        setGraphic(label);
                    }
                }
            };
        }
    }

    private static class EventFilterStringConverter<T> extends StringConverter<EventFilter> {
        @Override
        public String toString(EventFilter filter) {
            return filter.getLabel();
        }

        @Override
        public EventFilter fromString(String filterLabel) {
            return null;
        }
    }
}
