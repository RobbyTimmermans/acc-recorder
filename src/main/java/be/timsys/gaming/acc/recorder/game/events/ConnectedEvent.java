package be.timsys.gaming.acc.recorder.game.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ConnectedEvent {

    private final String ip;
    private final int port;
}
