package be.timsys.gaming.acc.recorder.spring;

import lombok.RequiredArgsConstructor;
import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
public class DbInit {

    private final DataSource dataSource;

    @PostConstruct
    public void flyway() {
        var flyway = Flyway.configure()
                .baselineOnMigrate(true)
                .dataSource(dataSource)
                .load();

        flyway.migrate();
    }
}
