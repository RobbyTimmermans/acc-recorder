package be.timsys.gaming.acc.recorder.game.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Getter
@RequiredArgsConstructor
public class GameError {

    private final Exception exception;
    private final LocalDateTime time = LocalDateTime.now();
}
