package be.timsys.gaming.acc.recorder.config;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static org.apache.commons.io.FileUtils.byteCountToDisplaySize;
import static org.apache.commons.io.FileUtils.sizeOfDirectory;

@Component
@RequiredArgsConstructor
public class ConfigDialog extends Dialog<Config> {

    private final ConfigService configService;
    private TextField server;
    private TextField port;
    private TextField user;
    private TextField pwd;
    private Spinner<Integer> timeout;
    private Spinner<Integer> readRetry;
    private Config config;

    @PostConstruct
    public void init() {
        config = configService.getConfig();
        var grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);

        server = new TextField(config.getServer());
        port = new TextField(String.valueOf(config.getPort()));
        user = new TextField(config.getUser());
        pwd = new TextField(config.getPwd());
        timeout = new Spinner<>(1000, 50000, config.getReadTimeoutMs(), 1000);
        readRetry = new Spinner<>(4000, 50000, config.getRetryDelayOnGameNotReachableMs(), 1000);

        grid.addRow(0, new Label("Installation-id"), new Label(config.getInstallationId()));
        grid.addRow(1, new Label("ACC host and port"), server, port);
        grid.addRow(2, new Label("User to connect to ACC"), user, pwd);
        grid.addRow(3, new Label("Timout (ms)"), timeout);
        grid.addRow(4, new Label("Check if game is running every (ms)"), readRetry);
        grid.addRow(5, new Label("Folder to save session data"), new Label(config.getDataFolder().toString()), new Label("currently using " + getUsedFileSize(config)));
        grid.addRow(6, new Label("Folder to save pdf export files"), new Label(config.getExportFolder().toString()));

        setTitle("Settings");
        setHeaderText("User and password should match your broadcasting.json");
        getDialogPane().setContent(grid);
        getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CLOSE);

        setResultConverter(button -> {
            if (button == ButtonType.APPLY) {
                config.setServer(server.getText());
                config.setPort(Integer.parseInt(port.getText()));
                config.setUser(user.getText());
                config.setPwd(pwd.getText());
                config.setReadTimeoutMs(timeout.getValue());
                config.setRetryDelayOnGameNotReachableMs(readRetry.getValue());
                return config;
            }
            return null;
        });
    }

    private String getUsedFileSize(Config config) {
        return byteCountToDisplaySize(sizeOfDirectory(config.getDataFolder().toFile()));
    }
}
