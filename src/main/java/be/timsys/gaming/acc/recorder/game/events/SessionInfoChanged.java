package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.SessionInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SessionInfoChanged {

    private final SessionInfo sessionInfo;

}
