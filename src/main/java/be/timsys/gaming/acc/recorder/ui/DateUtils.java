package be.timsys.gaming.acc.recorder.ui;

import java.time.*;

public class DateUtils {

    public static String formatDate(LocalDateTime localDateTime) {
        return formatDate(localDateTime.atOffset(ZoneOffset.of(ZoneId.systemDefault().getId())));
    }

    public static String formatDate(OffsetDateTime date) {
        return String.format("%s - %s", getDateString(date), String.format("%tR", date));
    }

    private static String getDateString(OffsetDateTime date) {
        var dateString = "";
        var today = LocalDate.now();
        if (date.toLocalDate().isEqual(today)) {
            dateString = "Today";
        } else if (date.toLocalDate().isEqual(today.minusDays(1))) {
            dateString = "Yesterday";
        } else {
            dateString = String.format("%ta, %td %tb", date, date, date);
        }
        return dateString;
    }
}
