package be.timsys.gaming.acc.recorder.ui.session.detail;

import java.io.File;

public interface ExportAction {

    File execute();
}
