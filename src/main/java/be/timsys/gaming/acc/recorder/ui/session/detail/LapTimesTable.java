package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.DriverColumnFormatter;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static be.timsys.gaming.acc.recorder.ui.TableUtils.addColumn;
import static be.timsys.gaming.acc.recorder.ui.TableUtils.addSectorColumn;

@Component
@Scope("prototype")
@RequiredArgsConstructor
public class LapTimesTable extends BorderPane {

    private final LapDetail lapDetail;
    private TableView<Lap> tableView;
    private Session session;
    private SessionEntry entry;

    @PostConstruct
    public void init() {
        tableView = new TableView<>();

        addColumn(tableView, "L", "lapNumber", 4);
        addColumn(tableView, "Driver", "driver", 7, new DriverColumnFormatter<>());
        addColumn(tableView, "Totaltime", "formattedTime", 18);
        addSectorColumn(tableView, "S1", 0, 16);
        addSectorColumn(tableView, "S2", 1, 16);
        addSectorColumn(tableView, "S3", 2, 16);
        addLapIndicatorColumn(tableView, 16);

        this.setCenter(tableView);
        lapDetail.setPrefWidth(320);
        lapDetail.setMaxWidth(450);
        this.setRight(lapDetail);

        tableView.getSelectionModel().getSelectedItems().addListener((ListChangeListener<Lap>) c -> c.getList().forEach(l -> lapDetail.setLap(session, entry, l)));
    }

    private void addLapIndicatorColumn(TableView<Lap> tableView, double widthPercent) {
        TableColumn<Lap, Lap> column = new TableColumn<>("");
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(widthPercent / 100));
        column.setCellValueFactory(p -> new SimpleObjectProperty<>(p.getValue()));
        column.setCellFactory(new LapIndicatorCellFactory<>());
        tableView.getColumns().add(column);
    }

    public void setData(Session session, SessionEntry entry) {
        this.session = session;
        this.entry = entry;

        tableView.getItems().clear();
        if (entry.getLaps() != null) {
            tableView.getItems().addAll(entry.getLaps());
            tableView.getSelectionModel().select(0);
        }
    }

    private static class LapIndicatorCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {
        @Override
        public TableCell<S, T> call(TableColumn<S, T> param) {
            return new TableCell<>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        var container = new FlowPane();
                        container.setPrefHeight(10);
                        container.setHgap(5);
                        var lap = (Lap) item;
                        if (!lap.isValid()) {
                            container.getChildren().add(new Indicator("red", "Invalid lap"));
                        }
                        if (lap.isValid()) {
                            container.getChildren().add(new Indicator("limegreen", "Valid lap"));
                        }
                        if (lap.isFastest()) {
                            container.getChildren().add(new Indicator("mediumpurple", "Fastest lap"));
                        }
                        if (lap.isInLap()) {
                            container.getChildren().add(new Indicator("gray", "Pit In", "IN", "17"));
                        }
                        if (lap.isOutLap()) {
                            container.getChildren().add(new Indicator("gray", "Pit Out", "OUT", "33"));
                        }
                        setGraphic(container);
                    }
                }
            };
        }
    }
}
