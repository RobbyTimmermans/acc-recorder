package be.timsys.gaming.acc.recorder.ui.session;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.util.List;

public class TopBar extends GridPane {

    private final Label title;

    public TopBar(List<Node> leftNodes, String titleText, List<Node> rightNodes) {
        title = new Label(titleText);
        title.setStyle("-fx-font-size: 130%");

        var right = new FlowPane();
        if (rightNodes != null) {
            rightNodes.forEach(r -> right.getChildren().add(r));
        }
        right.setAlignment(Pos.TOP_RIGHT);
        right.setHgap(5);
        right.setPrefWidth(100);
        right.setColumnHalignment(HPos.RIGHT);

        var left = new FlowPane();
        left.setPrefWidth(100);
        if (leftNodes != null) {
            leftNodes.forEach(l -> left.getChildren().add(l));
        }
        left.setHgap(5);

        setPadding(new Insets(5, 10, 5, 10));

        setHgrow(left, Priority.ALWAYS);
        setHgrow(title, Priority.ALWAYS);
        setHgrow(right, Priority.ALWAYS);
        setHalignment(left, HPos.LEFT);
        setHalignment(title, HPos.CENTER);
        setHalignment(right, HPos.RIGHT);

        setStyle("-fx-border-color: black");

        add(left, 0, 0);
        add(title, 1, 0);
        add(right, 2, 0);
    }

    public void setTitle(String titleText) {
        title.setText(titleText);
    }
}
