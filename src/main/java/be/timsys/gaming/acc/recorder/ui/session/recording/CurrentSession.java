package be.timsys.gaming.acc.recorder.ui.session.recording;

import be.timsys.gaming.acc.recorder.game.events.SessionInfoChanged;
import be.timsys.gaming.acc.recorder.game.events.SessionStandingsUpdated;
import be.timsys.gaming.acc.recorder.game.standings.SessionStandings;
import be.timsys.gaming.acc.recorder.ui.session.SessionInfoBar;
import be.timsys.gaming.acc.recorder.ui.session.SessionStandingsTable;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class CurrentSession extends BorderPane {

   private final SessionInfoBar sessionInfoBar;
   private final WeatherInfoBar weatherInfoBar;
   private final SessionStandingsTable sessionStandingsTable;

    @PostConstruct
    void init() {
        setTop(new VBox(sessionInfoBar, weatherInfoBar));
        setCenter(sessionStandingsTable);
    }

    void clear() {
        sessionStandingsTable.setData(new SessionStandings());
    }

    @EventListener
    public void on(SessionStandingsUpdated sessionStandingsUpdated) {
        sessionStandingsTable.setData(sessionStandingsUpdated.getSessionStandings());
    }

    @EventListener
    public void onSessionInfo(SessionInfoChanged sessionInfoChanged) {
        sessionInfoBar.setSessionInfo(sessionInfoChanged.getSessionInfo());
        weatherInfoBar.setSessionInfo(sessionInfoChanged.getSessionInfo());
    }
}
