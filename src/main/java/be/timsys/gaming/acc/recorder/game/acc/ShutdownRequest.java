package be.timsys.gaming.acc.recorder.game.acc;


import be.timsys.gaming.acc.recorder.game.acc.StructWriter;

import java.io.IOException;

 class ShutdownRequest {

    private int connectionId;

    ShutdownRequest(int connectionId) {
        this.connectionId = connectionId;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(5);
        structWriter.writeByte((byte) 9);
        structWriter.writeInt(connectionId);

        return structWriter.toByteArray();
    }
}
