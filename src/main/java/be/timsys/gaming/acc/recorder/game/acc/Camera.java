package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
class Camera {

    private final String name;
}
