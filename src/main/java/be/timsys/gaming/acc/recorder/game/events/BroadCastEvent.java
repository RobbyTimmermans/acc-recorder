package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType;
import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BroadCastEvent {

    private String sessionId;
    private short carIndex;
    private String description;
    private TimeOfDay timeOfDay;
    private OffsetDateTime realTime;
    private BroadCastMessageType type;
}
