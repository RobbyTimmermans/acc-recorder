package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static java.math.RoundingMode.DOWN;

@Getter
@NoArgsConstructor
public class TimeOfDay implements  Comparable<TimeOfDay> {
    public static final BigDecimal SECONDS_IN_HOUR = BigDecimal.valueOf(3600);
    public static final BigDecimal SECONDS_IN_MINUTE = BigDecimal.valueOf(60);

    private int hour;
    private int minutes;
    private int seconds;
    private Double timeOfDayInSeconds;

    public TimeOfDay(Double timeOfDayInSeconds) {
        this.timeOfDayInSeconds = timeOfDayInSeconds;

        var timeInSeconds = BigDecimal.valueOf(timeOfDayInSeconds);
        hour = timeInSeconds.divide(SECONDS_IN_HOUR, DOWN).setScale(0, DOWN).intValue();
        minutes = timeInSeconds.remainder(SECONDS_IN_HOUR).divide(SECONDS_IN_MINUTE, DOWN).setScale(0, DOWN).intValue();
        seconds = timeInSeconds.remainder(SECONDS_IN_HOUR).remainder(SECONDS_IN_MINUTE).setScale(0, DOWN).intValue();
    }

    public String getFormattedTime() {
        return String.format("%s:%02d:%02d", hour, minutes, seconds);
    }

    public String getFormattedTimeHM() {
        return String.format("%s:%02d", hour, minutes);
    }

    @Override
    public int compareTo(TimeOfDay o) {
        return timeOfDayInSeconds.compareTo(o.timeOfDayInSeconds);
    }
}
