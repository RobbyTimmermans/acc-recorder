package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@RequiredArgsConstructor
class CameraSet {

    private final String name;
    private byte cameraCount;
    private List<Camera> cameras;

    void add(Camera camera) {
        if (cameras == null) {
            cameras = new ArrayList<>();
        }
        cameras.add(camera);
    }
}
