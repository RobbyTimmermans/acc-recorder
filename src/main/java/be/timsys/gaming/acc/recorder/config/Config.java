package be.timsys.gaming.acc.recorder.config;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.file.Path;

@Data
@NoArgsConstructor
public class Config {

    private String installationId;
    private String server;
    private int port;
    private String user;
    private String pwd;
    private int readTimeoutMs;
    private int retryDelayOnGameNotReachableMs;
    private Path exportFolder;
    private Path dataFolder;
}
