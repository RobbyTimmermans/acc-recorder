package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.SessionEvent;
import be.timsys.gaming.acc.recorder.ui.DriverColumnFormatter;
import be.timsys.gaming.acc.recorder.ui.EventTypeFormatter;
import be.timsys.gaming.acc.recorder.ui.TimeOfDayColumnFormatter;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static be.timsys.gaming.acc.recorder.ui.TableUtils.addColumn;

@Component
@Scope("prototype")
@RequiredArgsConstructor
public class EntryEventsTable extends BorderPane {

    private TableView<SessionEvent> tableView;

    @PostConstruct
    public void init() {
        tableView = new TableView<>();

        addColumn(tableView, "Time", "timeOfDay", 8, new TimeOfDayColumnFormatter<>());
        addColumn(tableView, "L", "lapNumber", 3);
        addColumn(tableView, "Driver", "driver", 5, new DriverColumnFormatter<>());
        addColumn(tableView, "Event", "type", 25, new EventTypeFormatter<>());
        addColumn(tableView, "Description", "description", 32);


        this.setCenter(tableView);
    }

    public void setData(SessionEntry entry) {
        tableView.getItems().clear();
        if (entry.getEvents() != null) {
            tableView.getItems().addAll(entry.getEvents());
        }
    }
}
