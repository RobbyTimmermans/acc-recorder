package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.game.Lap;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.HorizontalAlignment;
import org.apache.commons.lang3.StringUtils;

class PdfUtils {
    static Paragraph getSplits(Lap l, int i) {
        return l.getSplits(i).map(s -> getTime(s.getFormattedSplit(), s.isFastest()))
                .orElse(new Paragraph(""));
    }

    static Paragraph getSplitsWithoutColor(Lap l, int i) {
        return l.getSplits(i).map(s -> getTime(s.getFormattedSplit(), false))
                .orElse(new Paragraph(""));
    }

    static Paragraph getTime(String time, boolean isFastest) {
        var text = new Paragraph(time);
        text.setHorizontalAlignment(HorizontalAlignment.CENTER);
        if (isFastest) {
            text.setUnderline();
            text.setFontColor(ColorConstants.WHITE);
            text.setBackgroundColor(new DeviceRgb(147, 112, 219));
        }
        return text;
    }

    static String makeFileNameUsable(String string) {
        var result = string.toLowerCase();
        result = StringUtils.replace(result, " ", "_");
        return result;
    }
}
