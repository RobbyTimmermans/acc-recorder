package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.SessionEvent;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.UnitValue;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import static be.timsys.gaming.acc.recorder.export.PdfUtils.getSplits;
import static be.timsys.gaming.acc.recorder.export.PdfUtils.getTime;
import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
class SessionEntryPdf {

    private final SessionEntry sessionEntry;

    Div lapTimesTable() {
        var columnWidths = UnitValue.createPercentArray(new float[]{4, 2, 3, 7, 7, 7, 7, 10, 9, 6});
        Table table = new Table(columnWidths)
                .useAllAvailableWidth()
                .addHeaderCell("Time")
                .addHeaderCell("L")
                .addHeaderCell("D")
                .addHeaderCell("TotalTime")
                .addHeaderCell("S1")
                .addHeaderCell("S2")
                .addHeaderCell("S3")
                .addHeaderCell("Temp")
                .addHeaderCell("Weather")
                .addHeaderCell("");

        if (sessionEntry.getLaps() != null) {
            sessionEntry.getLaps().forEach(l -> addLap(table, l));
        }

        var heading = new Paragraph("Laptimes");
        return new Div().add(heading).add(table);
    }

    private void addLap(Table table, Lap l) {
        table.addCell(l.getGameTimeOfDay().getFormattedTimeHM())
                .addCell(String.valueOf(l.getLapNumber()))
                .addCell(l.getDriver() != null ? l.getDriver().getShortName() : "")
                .addCell(getTime(l.getFormattedTime(), l.isFastest()))
                .addCell(getSplits(l, 0))
                .addCell(getSplits(l, 1))
                .addCell(getSplits(l, 2))
                .addCell(l.getWeather().getTempDescription())
                .addCell(l.getWeather().getWeatherDescription())
                .addCell(getIndicatorColumn(l));
    }

    Div getFullEntryInfo() {
        var entryInfo = new Div();
        var car = new Paragraph(String.format("#%s - %s", sessionEntry.getNumber(), sessionEntry.getCar()));
        car.setFontSize(12);

        if (StringUtils.isNotEmpty(sessionEntry.getTeamName())) {
            var teamName = new Text(String.format("\n%s", sessionEntry.getTeamName()));
            teamName.setFontSize(10);
            car.add(teamName);
        }

        var driverInfo = new Paragraph();
        driverInfo.setFontSize(10);
        sessionEntry.getDrivers().forEach(d -> {
            driverInfo.add(new Text(String.format("%s - %s (%s - %s)\n", d.getShortName(), d.getDriverFullName(), d.getNationality().getLabel(), d.getCup())));
        });

        entryInfo
                .add(car)
                .add(driverInfo);

        return entryInfo;
    }

    Table overallStats() {
        var stats = new Table(6);
        stats.addHeaderCell("").addHeaderCell("").addHeaderCell("Total").addHeaderCell("S1").addHeaderCell("S2").addHeaderCell("S3");

        ofNullable(sessionEntry.getFastestLap())
                .ifPresent(l -> stats
                        .addCell("Fastest lap")
                        .addCell(l.getDriver() != null ? l.getDriver().getShortName() : "")
                        .addCell(l.getFormattedTime())
                        .addCell(l.getFormattedSplits(0))
                        .addCell(l.getFormattedSplits(1))
                        .addCell(l.getFormattedSplits(2)));

        ofNullable(sessionEntry.getTheoreticalBestLap())
                .ifPresent(l -> stats
                        .addCell("Theoretical best")
                        .addCell("")
                        .addCell(l.getFormattedTime())
                        .addCell(l.getFormattedSplits(0))
                        .addCell(l.getFormattedSplits(1))
                        .addCell(l.getFormattedSplits(2)));

        stats.setMarginBottom(15);

        return stats;
    }

    Div sessionEvents() {
        var columnWidths = UnitValue.createPercentArray(new float[]{4, 2, 3, 20, 30});
        Table table = new Table(columnWidths)
                .useAllAvailableWidth()
                .addHeaderCell("Time")
                .addHeaderCell("L")
                .addHeaderCell("D")
                .addHeaderCell("Event")
                .addHeaderCell("Description");

        if (sessionEntry.getEvents() != null) {
            sessionEntry.getEvents().forEach(e -> addEvent(table, e));
        }

        var heading = new Paragraph("Events");
        return new Div().add(heading).add(table);
    }

    private void addEvent(Table table, SessionEvent e) {
        table.addCell(e.getTimeOfDay().getFormattedTimeHM())
                .addCell(String.valueOf(e.getLapNumber()))
                .addCell(e.getDriver() != null ? e.getDriver().getShortName() : "")
                .addCell(e.getType().getLabel())
                .addCell(e.getDescription());
    }


    private String getIndicatorColumn(Lap l) {
        var indicator = new ArrayList<String>();

        if (!l.isValid()) indicator.add("Invalid");
        if (l.isInLap()) indicator.add("IN");
        if (l.isOutLap()) indicator.add("OUT");
        if (l.isFastest()) indicator.add("F");

        return String.join(",", indicator);
    }
}
