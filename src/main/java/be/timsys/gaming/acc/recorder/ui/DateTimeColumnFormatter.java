package be.timsys.gaming.acc.recorder.ui;

import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.time.OffsetDateTime;

import static be.timsys.gaming.acc.recorder.ui.DateUtils.formatDate;

public class DateTimeColumnFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    @Override
    public TableCell<S, T> call(TableColumn<S, T> arg0) {
        return new TableCell<>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    OffsetDateTime date = (OffsetDateTime) item;
                    setGraphic(new Label(formatDate(date)));
                }
            }
        };
    }
}
