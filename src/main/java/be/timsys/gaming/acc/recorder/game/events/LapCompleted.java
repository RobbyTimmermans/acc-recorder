package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.Lap;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LapCompleted {

    private final String sessionId;
    private final short carId;
    private final Lap lap;
}
