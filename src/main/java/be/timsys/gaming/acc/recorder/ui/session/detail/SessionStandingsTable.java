package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.ui.session.SessionStandingsTable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class SessionDetailTable extends SessionStandingsTable {

    private final ApplicationEventPublisher publisher;

    @Override
    public void init() {
        super.init();
        tableView.getColumns().add(createActionColumn());
    }

    private TableColumn<SessionEntry, Short> createActionColumn() {
        TableColumn<SessionEntry, Short> column = new TableColumn<>("");
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(5d / 100));
        column.setCellValueFactory(new PropertyValueFactory<>("id"));
        column.setCellFactory(new Callback<>() {
            @Override
            public TableCell<SessionEntry, Short> call(TableColumn<SessionEntry, Short> param) {
                return new TableCell<>() {

                    @Override
                    protected void updateItem(Short entryId, boolean empty) {
                        super.updateItem(entryId, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            sessionStandings.find(entryId).ifPresent(e -> {
                                        var button = new Hyperlink("Detail");
                                        button.setOnAction(event -> publisher.publishEvent(SessionStandingsTableClicked.builder().sessionEntry(e).build()));
                                        setGraphic(button);
                                    }
                            );
                        }
                    }
                };
            }
        });
        return column;
    }

}
