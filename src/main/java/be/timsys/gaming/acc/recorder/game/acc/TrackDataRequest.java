package be.timsys.gaming.acc.recorder.game.acc;


import java.io.IOException;

class TrackDataRequest {
    private int connectionId;

    TrackDataRequest(int connectionId) {
        this.connectionId = connectionId;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 11);
        structWriter.writeInt(connectionId);

        return structWriter.toByteArray();
    }
}
