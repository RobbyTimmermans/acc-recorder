package be.timsys.gaming.acc.recorder.ui.session.detail;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

public class Indicator extends Label {

    public Indicator(String color, String tooltip) {
        this(color, tooltip, "7");
    }

    public Indicator(String color, String tooltip, String width) {
        setTooltip(new Tooltip(tooltip));
        setStyle(String.format("-fx-background-color: %s; -fx-text-fill: white; -fx-pref-width: %s", color, width));
        setPadding(new Insets(0,2,0,2));
    }

    public Indicator(String color, String tooltip, String label, String width) {
        this(color, tooltip, width);
        this.setText(label);
    }
}
