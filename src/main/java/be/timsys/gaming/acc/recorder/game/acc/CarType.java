package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

import static be.timsys.gaming.acc.recorder.game.acc.CarClass.GT3;
import static be.timsys.gaming.acc.recorder.game.acc.CarClass.GT4;

@Getter
public enum CarType {
    PORSCHE_991_GT3_R(0,"Porsche 991 GT3 R", 2018, GT3),
    MERCEDES_AMG_GT3(1,"Mercedes-AMG GT3", 2015, GT3),
    FERRARI_488_GT3(2,"Ferrari 488 GT3", 2018, GT3),
    AUDI_R8_LMS(3,"Audi_R8 LMS", 2015, GT3),
    LAMBORGHINI_HURACAN_GT3(4,"Lamborghini Huracan GT3", 2015, GT3),
    MCLAREN_650S_GT3(5,"McLaren 650S GT3", 2015, GT3),
    NISSAN_GT_R_NISMO_GT3_2018(6,"Nissan GT-R Nismo GT3", 2018, GT3),
    BMW_M6_GT3(7,"BMW M6 GT3", 2017, GT3),
    BENTLEY_CONTINENTAL_GT3_2018(8,"Bentley Continental GT3", 2018, GT3),
    PORSCHE_991_II_GT3_CUP(9,"Porsche 991 II GT3 Cup", 2017, GT3),
    NISSAN_GT_R_NISMO_GT3_2015(10,"Nissan GT-R Nismo GT3", 2015, GT3),
    BENTLEY_CONTINENTAL_GT3_2015(11,"Bentley Continental GT3", 2015, GT3),
    AMR_V12_VANTAGE_GT3(12,"AMR V12 Vantage GT3", 2013, GT3),
    REITER_ENGINEERING_R_EX_GT3(13,"Reiter Engineering R-EX GT3", 2017, GT3),
    EMIL_FREY_JAGUAR_GT3(14,"Emil Frey Jaguar GT3", 2012, GT3),
    LEXUS_RC_F_GT3(15,"Lexus RC F GT3", 2016, GT3),
    LAMBORGHINI_HURACAN_GT3_EVO(16,"Lamborghini Huracan GT3 Evo", 2019, GT3),
    HONDA_NSX_GT3(17,"Honda NSX GT3", 2017, GT3),
    LAMBORGHINI_HURACAN_SUPERTROFEO(18,"Lamborghini Huracan SuperTrofeo", 2015, GT3),
    AUDI_R8_LMS_EVO(19,"Audi R8 LMS Evo", 2019, GT3),
    AMR_V8_VANTAGE(20,"AMR V8 Vantage", 2019, GT3),
    HONDA_NSX_GT3_EVO(21,"Honda NSX GT3 Evo", 2019, GT3),
    MCLAREN_720S_GT3(22,"McLaren 720S GT3", 2019, GT3),
    PORSCHE_911_II_GT3_R(23,"Porsche 911 II GT3 R", 2019, GT3),
    FERRARI_488_GT3_EVO(24,"Ferrari 488 GT3 Evo", 2020, GT3),
    MERCEDES_AMG_GT3_EVO(25,"Mercedes AMG GT3", 2020, GT3),

    ALPINE_A110_GT4(50,"Alpine A110 GT4", 2018, GT4),
    ASTON_MARTIN_VANTAGE_GT4(51,"Aston Martin Vantage GT4", 2018, GT4),
    AUDI_R8_LMS_GT4(52,"Audi R8 LMS GT4", 2018, GT4),
    BMW_M4_GT4(53,"BMW M4 GT4", 2018, GT4),
    CHEVROLET_CAMARO_GT4(55,"Chevrolet Camaro GT4", 2017, GT4),
    GINETTA_G55_GT4(56,"Ginetta G55 GT4", 2012, GT4),
    KTM_X_BOW_GT4(57,"KTM X-Bow GT4", 2016, GT4),
    MASERATI_MC_GT4(58,"Maserati MC GT4", 2016, GT4),
    MCLAREN_570S_GT4(59,"McLaren 570S GT4", 2016, GT4),
    MERCEDES_AMG_GT4(60,"Mercedes AMG GT4", 2016, GT4),
    PORSCHE_718_CAYMAN_GT4_CLUBSPORT(61,"Porsche 718 Cayman GT4 Clubsport", 2019, GT4);

    private final byte id;
    private final String description;
    private final int year;
    private final CarClass carClass;

    CarType(int id, String description, int year, CarClass carClass) {
        this.id = (byte) id;
        this.description = description;
        this.year = year;
        this.carClass = carClass;
    }

    public static Optional<CarType> fromId(byte id) {
        return Arrays.stream(values()).filter(s -> s.id == id).findFirst();
    }

}
