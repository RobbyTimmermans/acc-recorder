package be.timsys.gaming.acc.recorder.ui.export;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static javafx.scene.control.TabPane.TabClosingPolicy.UNAVAILABLE;

@Component
@Scope("prototype")
public class ShareDialog extends Dialog<String> {

    private ImageUrlSharePane imageUrlSharePane;
    private TextSharePane textSharePane;

    @PostConstruct
    void init() {
        var container = new BorderPane();
        container.setCenter(createShareTabs());

        setTitle("Share");
        setHeaderText("How do you want to share?");

        getDialogPane().setContent(container);
        getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        setResizable(true);
        getDialogPane().setPrefSize(600,500);

    }

    public void setText(String text) {
        textSharePane.setText(text);
    }

    public void setImage(Image image) {
        imageUrlSharePane.setImage(image);
    }

    private TabPane createShareTabs() {
        imageUrlSharePane = new ImageUrlSharePane();
        textSharePane = new TextSharePane();

        var tabPane = new TabPane(new Tab("Image", imageUrlSharePane), new Tab("Text", textSharePane));
        tabPane.setTabClosingPolicy(UNAVAILABLE);
        return tabPane;
    }
}
