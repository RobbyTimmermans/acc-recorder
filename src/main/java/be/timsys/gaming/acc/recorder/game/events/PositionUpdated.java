package be.timsys.gaming.acc.recorder.game.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PositionUpdated {

    private final String sessionId;
    private final short carIndex;
    private final short oldPosition;
    private final short newPosition;

    public boolean gainedPosition() {
        return newPosition < oldPosition;
    }
}
