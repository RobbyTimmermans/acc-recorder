package be.timsys.gaming.acc.recorder.ui.session.overview;

import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.session.detail.SessionDetail;
import be.timsys.javafx.scene.ShowScreenCommand;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

import static be.timsys.gaming.acc.recorder.ui.TableUtils.addColumn;
import static be.timsys.gaming.acc.recorder.ui.TableUtils.addDateColumn;

@Component
@RequiredArgsConstructor
public class SessionsTable extends BorderPane {

    private final ApplicationEventPublisher eventPublisher;

    private TableView<Session> tableView;

    @PostConstruct
    public void init() {
        setCenter(createTable());
    }

    private TableView<Session> createTable() {
        tableView = new TableView<>();
        tableView.setPlaceholder(new Label("No previous recorded sessions"));

        addDateColumn(tableView, "Date", "created", 13);
        addColumn(tableView, "Track", "trackName", 52);
        addColumn(tableView, "Classes", "carClasses", 10, new CarClassFormatter());
        addColumn(tableView, "Session", "sessionType", 17);
        tableView.getColumns().add(createActionColumn());

        tableView.setRowFactory(tv -> {
            TableRow<Session> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    var session = row.getItem();
                    openSessionDetail(session.getId());
                }
            });
            return row;
        });

        return tableView;
    }

    private TableColumn<Session, String> createActionColumn() {
        TableColumn<Session, String> column = new TableColumn<>("");
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(6d / 100));
        column.setCellValueFactory(new PropertyValueFactory<>("id"));
        column.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Session, String> call(TableColumn<Session, String> param) {
                return new TableCell<>() {

                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            var button = new Hyperlink("Results");
                            button.setOnAction(event -> openSessionDetail(item));
                            setGraphic(button);
                        }
                    }
                };
            }
        });
        return column;
    }

    void setData(List<Session> sessions) {
        tableView.getItems().clear();
        tableView.getItems().addAll(sessions);
    }

    private void openSessionDetail(String sessionId) {
        eventPublisher.publishEvent(new ShowScreenCommand<>(SessionDetail.class.getName(), sessionId));
    }

    private static class CarClassFormatter implements Callback<TableColumn<Session, Object>, TableCell<Session, Object>> {
        @Override
        public TableCell<Session, Object> call(TableColumn<Session, Object> param) {
            return new TableCell<>() {

                @Override
                protected void updateItem(Object item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        var carClasses = (List<CarClass>) item;
                            var label = new Label(carClasses.stream().map(CarClass::name).collect(Collectors.joining(", ")));
                            setGraphic(label);
                    }
                }
            };
        }
    }
}
