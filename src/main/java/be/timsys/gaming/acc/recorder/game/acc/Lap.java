package be.timsys.gaming.acc.recorder.game.acc;

import be.timsys.gaming.acc.recorder.game.Split;
import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
class Lap implements Comparable<Lap> {

    private Integer laptimeInMs;
    private Duration laptime;
    private short carIndex;
    private short driverIndex;
    private List<Integer> splits;
    private boolean invalid;
    private boolean isValidForBest;
    private boolean isOutlap;
    private boolean isInlap;

    Lap(LittleEndianDataInputStream lil) throws IOException {
        this.laptimeInMs = lil.readInt();
        this.laptime = Duration.ofMillis(laptimeInMs);
        this.carIndex = lil.readShort();
        this.driverIndex = lil.readShort();
        this.splits = getSplits(lil);
        this.invalid = lil.readBoolean();
        this.isValidForBest = lil.readBoolean();
        this.isOutlap = lil.readBoolean();
        this.isInlap = lil.readBoolean();
    }

    private List<Integer> getSplits(LittleEndianDataInputStream lil) throws IOException {
        short splitCount = lil.readByte();

        List<Integer> splits = new ArrayList<>(splitCount);

        for (short i = 0; i < splitCount; i++) {
            splits.add(lil.readInt());
        }
        return splits;
    }

    @Override
    public String toString() {
        return "Lap{" +
                "laptimeInMs=" + laptimeInMs +
                ", laptime=" + laptimeInMs +
                ", carIndex=" + carIndex +
                ", driverIndex=" + driverIndex +
                ", splits=" + splits +
                ", invalid=" + invalid +
                ", isValidForBest=" + isValidForBest +
                ", isOutlap=" + isOutlap +
                ", isInlap=" + isInlap +
                '}';
    }

    @Override
    public int compareTo(Lap o) {
        return this.getLaptimeInMs().compareTo(o.getLaptimeInMs());
    }

    List<Split> toSplits() {
        if (splits != null) {
            return IntStream.range(0, splits.size())
                    .mapToObj(i -> new Split(i, splits.get(i), false))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }
}


