package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.config.ConfigService;
import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import be.timsys.gaming.acc.recorder.game.acc.SessionType;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.DateUtils;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.AreaBreakType;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.OffsetDateTime;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static be.timsys.gaming.acc.recorder.export.PdfUtils.makeFileNameUsable;

@Service
@RequiredArgsConstructor
public class ExportService {

    private final ConfigService configService;
    private final ImageExport imageExport;

    @SneakyThrows
    public File exportToPdf(Session session, SessionEntry sessionEntry) {
        var fileName = String.format("exp_%s_%s_%s_%s.pdf", sessionEntry.getNumber(), session.getSessionType(), getTrackId(session), getDate(session.getCreated()));
        var file = new File(configService.getConfig().getExportFolder().toFile(), fileName);

        try (var doc = new Document(createPdf(file))) {

            doc.setFontSize(8);
            doc.setFont(PdfFontFactory.createFont(StandardFonts.COURIER));

            var sessionEntryPdf = new SessionEntryPdf(sessionEntry);
            var sessionPdf = new SessionPdf(session);

            doc.add(sessionPdf.sessionInfo());
            doc.add(sessionEntryPdf.getFullEntryInfo());
            doc.add(sessionEntryPdf.overallStats());
            doc.add(sessionEntryPdf.lapTimesTable());
            doc.add(sessionEntryPdf.sessionEvents());
        }

        return file;
    }

    @SneakyThrows
    public File exportToPdf(Session session) {
        var fileName = String.format("exp_%s_%s_%s.pdf", session.getSessionType(), getTrackId(session), getDate(session.getCreated()));
        var file = new File(configService.getConfig().getExportFolder().toFile(), fileName);

        try (var doc = new Document(createPdf(file))) {
            doc.setFontSize(8);
            doc.setFont(PdfFontFactory.createFont(StandardFonts.COURIER));

            var sessionPdf = new SessionPdf(session);
            doc.add(sessionPdf.sessionInfo().setFontSize(12));

            if (SessionType.RACE.equals(session.getSessionType())) {
                doc.add(sessionPdf.resultsTable().setPaddingBottom(20));
            }

            if (!session.getCarClasses().isEmpty()) {
                session.getCarClasses().forEach(carClass -> addCarClass(doc, sessionPdf, carClass));
            } else {
                // backwards compatibility
                addCarClass(doc, sessionPdf, null);
            }

            if (session.getSessionStandings() != null && session.getSessionStandings().hasEntries()) {
                session.getSessionStandings().getSessionEntries().forEach(e -> {
                    var sessionEntryPdf = new SessionEntryPdf(e);
                    doc.add(sessionEntryPdf.getFullEntryInfo());
                    doc.add(sessionEntryPdf.overallStats());
                    doc.add(sessionEntryPdf.lapTimesTable());
                    doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                });
            }
        }

        return file;
    }

    private void addCarClass(Document doc, SessionPdf sessionPdf, CarClass carClass) {
        if (carClass != null) {
            doc.add(new Paragraph(carClass.name()).setFontSize(25));
        }

        doc.add(sessionPdf.fastestLaps(carClass).setPaddingBottom(20));
        doc.add(sessionPdf.incidents(carClass).setPaddingBottom(20));
        doc.add(sessionPdf.bestSessionLaps(carClass).setPaddingBottom(20));

        doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
    }

    public String exportToImageUrl(Session session, SessionEntry entry, Lap lap, double width, double height) {
        return imageExport.exportToImageUrl(session, entry, lap, width, height);
    }

    public String exportToString(Session session, SessionEntry entry, Lap lap) {
        return String.format("%s - %s - %s - %s (%s)\n" +
                        "Lap Time %s (%s)\n" +
                        "%s - %s\n" +
                        "Time of day %s",
                DateUtils.formatDate(session.getCreated()),
                session.getTrackName(),
                lap.getDriver().getDriverFullName(),
                entry.getCarType().getDescription(),
                entry.getCarType().getCarClass(),
                lap.getFormattedTime(),
                getSplits(lap),
                lap.getWeather().getTempDescription(),
                lap.getWeather().getWeatherDescription(),
                lap.getGameTimeOfDay().getFormattedTimeHM());
    }

    private String getSplits(Lap lap) {
        if (lap != null) {
            return IntStream.range(0, lap.getSplits().size())
                    .mapToObj(i -> String.format("S%s: %s", i + 1, lap.getFormattedSplits(i)))
                    .collect(Collectors.joining(", "));
        } else {
            return "";
        }
    }

    private String getTrackId(Session session) {
        if (session.getTrackName() != null) {
            return makeFileNameUsable(session.getTrackName());
        } else {
            return "";
        }
    }

    private String getDate(OffsetDateTime t) {
        return String.format("%td%tm%ty_%tH%tM%tS", t, t, t, t, t, t);
    }

    private PdfDocument createPdf(File file) throws FileNotFoundException {
        var pdf = new PdfDocument(new PdfWriter(file));
        pdf.getCatalog().put(PdfName.Creator, new PdfString("Robby Timmermans - Timsys bv"));
        pdf.addEventHandler(PdfDocumentEvent.END_PAGE, new PdfFooter());
        return pdf;
    }
}
