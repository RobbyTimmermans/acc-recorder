package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.session.Session;
import org.springframework.stereotype.Component;

import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class ImageExport {

    String exportToImageUrl(Session session, SessionEntry entry, Lap lap, double width, double height) {
        var lapImage = LapImage.builder()
                .entry(entry)
                .session(session)
                .lap(lap)
                .height(height)
                .width(width)
                .build();

        return lapImage.getUrl();
    }

    static String encodeString(String mark) {
        return Base64.getEncoder().encodeToString(mark.getBytes(UTF_8));
    }
}
