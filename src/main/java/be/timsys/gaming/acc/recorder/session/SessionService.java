package be.timsys.gaming.acc.recorder.session;

import be.timsys.gaming.acc.recorder.game.events.NewSessionStarted;
import be.timsys.gaming.acc.recorder.game.events.SessionInfoChanged;
import be.timsys.gaming.acc.recorder.game.events.SessionStandingsUpdated;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SessionService {

    private final SessionRepository sessionRepository;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final Logger logger;

    @Cacheable("sessionOverview")
    @Transactional(readOnly = true)
    public List<Session> findAll() {
        logger.info("Find all sessions");
        return sessionRepository.findAll(Sort.by("created").descending());
    }

    @Cacheable("sessions")
    @Transactional(readOnly = true)
    public Optional<Session> find(String sessionId) {
        logger.info("Find session {}", sessionId);
        return sessionRepository.findById(sessionId);
    }

    @Transactional
    @CacheEvict(value = "sessionOverview", allEntries = true)
    public void delete(String sessionId) {
        logger.info("Deleting session {}", sessionId);

        sessionRepository.findById(sessionId).ifPresent(s -> {
            try {
                Files.deleteIfExists(Path.of(s.getFileName()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            sessionRepository.deleteById(sessionId);
        });
    }

    @EventListener
    @Transactional
    @CacheEvict(value = "sessionOverview", allEntries = true)
    public void onSessionStarted(NewSessionStarted newSession) {
        sessionRepository.save(Session.builder()
                .id(newSession.getId())
                .created(OffsetDateTime.now())
                .lastModified(OffsetDateTime.now())
                .sessionType(newSession.getSessionType())
                .trackName(newSession.getTrackName())
                .trackId(newSession.getTrackId())
                .fileName(newSession.getFileName())
                .eventIndex((int) newSession.getEventIndex())
                .build()
        );

        publishSessionPersisted();
    }

    @EventListener
    @Transactional
    public void onSessionInfoChanged(SessionInfoChanged sessionInfoChanged) {
        var sessionInfo = sessionInfoChanged.getSessionInfo();
        getSession(sessionInfo.getSessionId()).ifPresent(session -> {
            if (session.getTrackName() == null || !session.getTrackName().equalsIgnoreCase(sessionInfo.getTrackName())) {
                session.setTrackName(sessionInfo.getTrackName());
                session.setTrackId(sessionInfo.getTrackId());
                sessionRepository.save(session);
                publishSessionPersisted();
            }
        });
    }

    @EventListener
    @Transactional
    public void on(SessionStandingsUpdated sessionStandingsUpdated) {
        getSession(sessionStandingsUpdated.getSessionId()).ifPresent(session -> {
            session.setSessionStandings(sessionStandingsUpdated.getSessionStandings());
            sessionRepository.save(session);
            publishSessionPersisted();
        });
    }

    private Optional<Session> getSession(String sessionId) {
        return sessionRepository.findById(sessionId);
    }

    private void publishSessionPersisted() {
        applicationEventPublisher.publishEvent(new SessionPersistedEvent());
    }
}
