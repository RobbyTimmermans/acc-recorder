package be.timsys.gaming.acc.recorder.export;

public interface ImageUrl {

    String getUrl();
}
