package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum Nationality {

    Any((short) 0, "Any", "??"),
    Italy((short) 1, "Italy", "IT"),
    Germany((short) 2, "Germany", "DE"),
    France((short) 3, "France", "FR"),
    Spain((short) 4, "Spain", "ES"),
    GreatBritain((short) 5, "Great Britain", "GB"),
    Hungary((short) 6, "Hungary", "HU"),
    Belgium((short) 7, "Belgium", "BE"),
    Switzerland((short) 8, "Switzerland", "CH"),
    Austria((short) 9, "Austria", "AT"),
    Russia((short) 10, "Russia", "RU"),
    Thailand((short) 11, "Thailand", "TH"),
    Netherlands((short) 12, "Netherlands", "NL"),
    Poland((short) 13, "Poland", "PL"),
    Argentina((short) 14, "Argentina", "AR"),
    Monaco((short) 15, "Monaco", "MC"),
    Ireland((short) 16, "Ireland", "IE"),
    Brazil((short) 17, "Brazil", "BR"),
    SouthAfrica((short) 18, "South Africa", "ZA"),
    PuertoRico((short) 19, "Puerto Rico", "PR"),
    Slovakia((short) 20, "Slovakia", "SK"),
    Oman((short) 21, "Oman", "OM"),
    Greece((short) 22, "Greece", "GR"),
    SaudiArabia((short) 23, "Saudi Arabia", "SA"),
    Norway((short) 24, "Norway", "NO"),
    Turkey((short) 25, "Turkey", "TR"),
    SouthKorea((short) 26, "South Korea", "KR"),
    Lebanon((short) 27, "Lebanon", "LB"),
    Armenia((short) 28, "Armenia", "AM"),
    Mexico((short) 29, "Mexico", "MX"),
    Sweden((short) 30, "Sweden", "SE"),
    Finland((short) 31, "Finland", "FI"),
    Denmark((short) 32, "Denmark", "DK"),
    Croatia((short) 33, "Croatia", "HR"),
    Canada((short) 34, "Canada", "CA"),
    China((short) 35, "China", "CN"),
    Portugal((short) 36, "Portugal", "PT"),
    Singapore((short) 37, "Singapore", "SG"),
    Indonesia((short) 38, "Indonesia", "ID"),
    USA((short) 39, "USA", "US"),
    NewZealand((short) 40, "New Zealand", "NZ"),
    Australia((short) 41, "Australia", "AU"),
    SanMarino((short) 42, "San Marino", "SM"),
    UAE((short) 43, "UAE", "AE"),
    Luxembourg((short) 44, "Luxembourg", "LU"),
    Kuwait((short) 45, "Kuwait", "KW"),
    HongKong((short) 46, "Hong Kong", "HK"),
    Colombia((short) 47, "Colombia", "CO"),
    Japan((short) 48, "Japan", "JP"),
    Andorra((short) 49, "Andorra", "AD"),
    Azerbaijan((short) 50, "Azerbaijan", "AZ"),
    Bulgaria((short) 51, "Bulgaria", "BG"),
    Cuba((short) 52, "Cuba", "CU"),
    CzechRepublic((short) 53, "Czech Republic", "CZ"),
    Estonia((short) 54, "Estonia", "EE"),
    Georgia((short) 55, "Georgia", "GE"),
    India((short) 56, "India", "IN"),
    Israel((short) 57, "Israel", "IL"),
    Jamaica((short) 58, "Jamaica", "JM"),
    Latvia((short) 59, "Latvia", "LV"),
    Lithuania((short) 60, "Lithuania", "LT"),
    Macau((short) 61, "Macau", "MO"),
    Malaysia((short) 62, "Malaysia", "MY"),
    Nepal((short) 63, "Nepal", "NP"),
    NewCaledonia((short) 64, "New Caledonia", "NC"),
    Nigeria((short) 65, "Nigeria", "NG"),
    NorthernIreland((short) 66, "Northern Ireland", "IE"),
    PapuaNewGuinea((short) 67, "Papua New Guinea", "PG"),
    Philippines((short) 68, "Philippines", "PH"),
    Qatar((short) 69, "Qatar", "QA"),
    Romania((short) 70, "Romania", "RO"),
    Scotland((short) 71, "Scotland", "SC"),
    Serbia((short) 72, "Serbia", ""),
    Slovenia((short) 73, "Slovenia", "CS"),
    Taiwan((short) 74, "Taiwan", "TW"),
    Ukraine((short) 75, "Ukraine", "UA"),
    Venezuela((short) 76, "Venezuela", "VE"),
    Wales((short) 77, "Wales", "WA");

    Nationality(short id, String label, String abbreviation) {
        this.id = id;
        this.label = label;
        this.abbreviation = abbreviation;
    }

    private final short id;
    private final String label;
    private final String abbreviation;

    public static Nationality fromId(short id) {
        return Stream.of(values()).filter(cl -> cl.id == id).findFirst().orElse(Any);
    }
}
