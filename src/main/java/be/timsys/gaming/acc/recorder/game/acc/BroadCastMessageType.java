package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
public enum BroadCastMessageType {
    NONE((byte) 0, "None"), GREEN_FLAG((byte) 1, "GR"), SESSION_OVER((byte) 2, "Session Over"), PENALTY_COMM_MSG((byte) 3, "Penalty Message"),
    ACCIDENT((byte) 4, "Incident"), LAP_COMPLETED((byte) 5, "Lap Completed"), BEST_SESSION_LAP((byte) 6, "Best Session Lap"), BEST_PERSONAL_LAP((byte) 7, "Best Personal Lap");

    BroadCastMessageType(byte id, String label) {
        this.id = id;
        this.label = label;
    }

    private final byte id;
    private final String label;

    public static Optional<BroadCastMessageType> fromId(byte id) {
        return Stream.of(values()).filter(cl -> cl.id == id).findFirst();
    }
}
