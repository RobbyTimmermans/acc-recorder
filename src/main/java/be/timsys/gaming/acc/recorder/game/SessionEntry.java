package be.timsys.gaming.acc.recorder.game;

import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import be.timsys.gaming.acc.recorder.game.acc.CarType;
import be.timsys.gaming.acc.recorder.game.acc.Cup;
import be.timsys.gaming.acc.recorder.game.acc.Nationality;
import lombok.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.left;
import static org.apache.commons.lang3.StringUtils.upperCase;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionEntry {

    private short id;
    private short position;
    private String number;
    private String car;
    private CarType carType;
    private String teamName;
    private Nationality nationality;
    private String driver;
    private Driver currentDriver;
    private List<Driver> drivers;
    private Lap fastestLap;
    private TheoreticalBest theoreticalBestLap;
    private List<Lap> laps;
    private List<SessionEvent> events;

    public List<Driver> getDrivers() {
        if (drivers == null || drivers.isEmpty()) {
            return Collections.singletonList(new Driver("", "", upperCase(left(this.driver, 3)), Cup.OVERALL, this.nationality == null ? Nationality.Any : this.nationality, this.driver));
        } else {
            return drivers;
        }
    }

    public void addLap(Lap lap) {
        if (laps == null) {
            laps = new ArrayList<>();
        }
        laps.add(lap);
        calculateFastestLap();
        calculateTheoreticalBest();
    }

    public void addSessionEvent(SessionEvent event) {
        if (events == null) {
            events = new ArrayList<>();
        }
        events.add(event);
    }

    public void calculateTheoreticalBest() {
        TheoreticalBest theo = new TheoreticalBest();

        int numberOfSplits = laps.stream().filter(l -> l.getSplits().size() > 0)
                .findFirst()
                .map(l -> l.getSplits().size())
                .orElse(0);

        for (int splitIndex = 0; splitIndex < numberOfSplits; splitIndex++) {
            findBestSplit(splitIndex).ifPresent(theo::addSplit);
        }

        this.theoreticalBestLap = theo;
    }

    public Optional<Split> findBestSplit(int i) {
        var bestSplit = laps.stream().filter(l -> l.getSplits().size() > i)
                .filter(Lap::isValid)
                .filter(l -> l.getLaptimeInMs() > 0)
                .map(l -> l.getSplits().get(i))
                .filter(splits -> splits.getTimeInMilliSec() < Integer.MAX_VALUE)
                .min(Split::compareTo);

        bestSplit.ifPresent(b -> {
            laps.stream().map(l -> l.getSplits(i))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(s -> s.setFastest(false));

            laps.stream().map(l -> l.getSplits(i))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .filter(s -> s.getTimeInMilliSec().equals(b.getTimeInMilliSec()))
                    .findFirst()
                    .ifPresent(s -> s.setFastest(true));
        });

        return bestSplit;
    }

    public void calculateFastestLap() {
        fastestLap = laps.stream()
                .filter(Lap::isValid)
                .min(Lap::compareTo)
                .orElse(null);

        if (fastestLap != null) {
            laps.forEach(l -> l.setFastest(false));
            laps.stream().filter(l -> l.getLapNumber().equals(fastestLap.getLapNumber()))
                    .findFirst()
                    .ifPresent(l -> l.setFastest(true));
        }
    }

    public Lap getLastLap() {
        if (laps != null) {
            return laps.get(laps.size() - 1);
        }
        return null;
    }

    public int getNumberOfLaps() {
        if (laps != null) {
            return laps.size();
        }
        return 0;
    }

    public CarClass getCarClass() {
        return carType != null ? carType.getCarClass() : null;
    }
}
