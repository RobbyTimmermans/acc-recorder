package be.timsys.gaming.acc.recorder.analytics;

import be.timsys.gaming.acc.recorder.api.AccRecorderApi;
import be.timsys.gaming.acc.recorder.config.ConfigService;
import be.timsys.gaming.acc.recorder.game.events.ConnectedEvent;
import be.timsys.javafx.scene.ShowScreenCommand;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
public class AnalyticsService {

    private final ConfigService configService;
    private final ObjectMapper objectMapper;
    private final AccRecorderApi accRecorderApi;
    private String currentAccRecorderSessionId;

    @Async
    @SneakyThrows
    public void startSession() {
        var sessionStarted = accRecorderApi.post("acc-session", new AccSessionStartRequest(getInstallationId()), this::getSession);

        this.currentAccRecorderSessionId = sessionStarted.getSessionId();
    }

    @Async
    @SneakyThrows
    public void trackAction(String action) {
        accRecorderApi.post(format("acc-session/%s/action", currentAccRecorderSessionId), new GuiAction(action), this::getAction);
    }

    @EventListener
    public void on(ShowScreenCommand<?> command) {
        trackAction(command.getScreenId());
    }

    @EventListener
    public void on(ConnectedEvent connectedEvent) {
        trackAction("Connected");
    }

    private String getInstallationId() {
        return configService.getConfig().getInstallationId();
    }

    @SneakyThrows
    private AccSession getSession(String json) {
        return objectMapper.readValue(json, AccSession.class);
    }

    private String getAction(String s) {
        return s;
    }
}
