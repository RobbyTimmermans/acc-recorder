package be.timsys.gaming.acc.recorder.game.acc;

import be.timsys.gaming.acc.recorder.game.acc.AccMessage;
import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

@Getter
@ToString
class Connection extends AccMessage {

    private int connectionId;
    private boolean isSuccess;
    private boolean isReadOnly;
    private String error;

    Connection(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.connectionId = lil.readInt();
        this.isSuccess = lil.readByte() > 0;
        this.isReadOnly = lil.readByte() == 0;

        this.error = readString(lil);
    }
}
