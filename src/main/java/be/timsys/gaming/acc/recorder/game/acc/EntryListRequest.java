package be.timsys.gaming.acc.recorder.game.acc;


import be.timsys.gaming.acc.recorder.game.acc.StructWriter;

import java.io.IOException;

class EntryListRequest {
    private int connectionId;

    EntryListRequest(int connectionId) {
        this.connectionId = connectionId;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 10);
        structWriter.writeInt(connectionId);

        return structWriter.toByteArray();
    }
}
