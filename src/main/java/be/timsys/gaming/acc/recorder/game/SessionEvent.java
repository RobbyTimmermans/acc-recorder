package be.timsys.gaming.acc.recorder.game;

import be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType;
import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import lombok.*;

import java.time.OffsetDateTime;

@Getter
// TODO remove setter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionEvent {

    private String description;
    private TimeOfDay timeOfDay;
    private OffsetDateTime realTime;
    private BroadCastMessageType type;
    private Driver driver;
    private String car;
    private String number;
    private int lapNumber;
}
