package be.timsys.gaming.acc.recorder.ui.session;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.standings.SessionStandings;
import be.timsys.gaming.acc.recorder.ui.Action;
import be.timsys.gaming.acc.recorder.ui.DriversColumnFormatter;
import be.timsys.gaming.acc.recorder.ui.NationalityColumnFormatter;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.stream.Collectors;

import static be.timsys.gaming.acc.recorder.ui.TableUtils.addColumn;
import static be.timsys.gaming.acc.recorder.ui.TableUtils.addLapColumn;

@Component
@Scope("prototype")
public class SessionStandingsTable extends BorderPane {

    protected TableView<SessionEntry> tableView;
    protected SessionStandings sessionStandings;

    @PostConstruct
    public void init() {
        setCenter(createTable());
    }

    private TableView<SessionEntry> createTable() {
        tableView = new TableView<>();
        tableView.setPlaceholder(new Label("No entries yet"));

        addColumn(tableView, "P", "position", 3);
        addColumn(tableView, "Nat", "nationality", 3, new NationalityColumnFormatter<>());
        addColumn(tableView, "N", "number", 3);
        addColumn(tableView, "Drivers", "drivers", 25, new DriversColumnFormatter<>());
        addColumn(tableView, "Car", "car", 20);
        addColumn(tableView, "Class", "carClass", 15);
        addLapColumn(tableView, "Fastest lap", "fastestLap", 8);
        addLapColumn(tableView, "Theoretical best lap", "theoreticalBestLap", 8);
        addLapColumn(tableView, "Last lap", "lastLap", 8);

        return tableView;
    }

    public void setOnDoubleClick(Action<SessionEntry> action) {
        tableView.setRowFactory(tv -> {
            TableRow<SessionEntry> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    action.execute(row.getItem());
                }
            });
            return row;
        });
    }


    public void setData(SessionStandings sessionStandings) {
        this.sessionStandings = sessionStandings;
        tableView.getItems().clear();
        if (sessionStandings != null && sessionStandings.hasEntries()) {
            tableView.getItems().addAll(sessionStandings.getSessionEntries().stream()
                    .sorted(Comparator.comparing(SessionEntry::getPosition))
                    .collect(Collectors.toList()));
        }
    }
}
