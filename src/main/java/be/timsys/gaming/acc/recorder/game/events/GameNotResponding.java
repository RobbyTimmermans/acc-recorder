package be.timsys.gaming.acc.recorder.game.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GameNotResponding {

    private final String reason;
    private final Throwable cause;
}
