package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.Driver;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.List;
import java.util.stream.Collectors;

public class DriversColumnFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    @Override
    public TableCell<S, T> call(TableColumn<S, T> arg0) {
        return new TableCell<>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    var drivers = (List<Driver>) item;
                    var label = new Label(drivers.stream().map(Driver::getDriverFullName).collect(Collectors.joining(" - ")));
                    setGraphic(label);
                }
            }
        };
    }
}
