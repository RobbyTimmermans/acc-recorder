package be.timsys.gaming.acc.recorder.game.acc;

import be.timsys.gaming.acc.recorder.config.ConfigService;
import be.timsys.gaming.acc.recorder.game.commands.DisconnectedEvent;
import be.timsys.gaming.acc.recorder.game.events.NewSessionStarted;
import be.timsys.gaming.acc.recorder.game.events.RecordingStarted;
import be.timsys.gaming.acc.recorder.game.events.RecordingStopped;
import be.timsys.gaming.acc.recorder.game.events.StatusChanged;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

//@Service
@RequiredArgsConstructor
public class Recorder {

    private final ConfigService configService;
    private final Logger logger;
    private final ApplicationEventPublisher publisher;

    private File dumpFile;
    private FileOutputStream fos;
    private SessionType sessionType;
    private String eventIndex;
    private String sessionIndex;

    @EventListener
    public void onNewSession(NewSessionStarted newSessionStarted) {
        logger.info("new session started");
        this.sessionType = newSessionStarted.getSessionType();
        this.eventIndex = String.valueOf(newSessionStarted.getEventIndex());
        this.sessionIndex = String.valueOf(newSessionStarted.getSessionIndex());

        closeCurrentFile();
        startNewFile(newSessionStarted.getId(), newSessionStarted.getFileName());
        publisher.publishEvent(new StatusChanged(String.format("New session started %s", newSessionStarted.getSessionType())));
    }

    @SneakyThrows
    private void startNewFile(String id, String fileName) {
        dumpFile = new File(configService.getConfig().getDataFolder().toFile(), fileName);
        fos = new FileOutputStream(dumpFile, true);

        logger.info("Starting to record " + dumpFile);

        publisher.publishEvent(new RecordingStarted(id, dumpFile));
        publisher.publishEvent(new StatusChanged(String.format("Start recording to file %s", dumpFile.getAbsolutePath())));
    }

    @SneakyThrows
    private void closeCurrentFile() {
        if (fos != null) {
            fos.close();
            fos = null;
        }
    }

    @EventListener
    public void onAccMessage(AccMessage accMessage) {
        try {
            if (fos != null) {
                fos.write(accMessage.getRawMessage());
            }
        } catch (IOException e) {
            var message = String.format("Error while writing file %s", e.getMessage());
            logger.error(message, e);
            publisher.publishEvent(new StatusChanged(message));
        }
    }

    @EventListener
    public void onDisconnect(DisconnectedEvent disconnectedEvent) {
        closeCurrentFile();
        publisher.publishEvent(new RecordingStopped());
    }
}
