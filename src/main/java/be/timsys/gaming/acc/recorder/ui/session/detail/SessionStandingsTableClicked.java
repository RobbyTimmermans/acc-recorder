package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SessionStandingsTableClicked {

    private final SessionEntry sessionEntry;
}
