package be.timsys.gaming.acc.recorder.game;

import be.timsys.gaming.acc.recorder.game.acc.Cup;
import be.timsys.gaming.acc.recorder.game.acc.Nationality;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Driver {
    private String firstName;
    private String lastName;
    private String shortName;
    private Cup cup;
    private Nationality nationality;
    private String driverFullName;
}
