package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.Driver;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DriverChanged {

    private final String sessionId;
    private final short carIndex;
    private final Driver oldDriver;
    private final Driver newDriver;
}
