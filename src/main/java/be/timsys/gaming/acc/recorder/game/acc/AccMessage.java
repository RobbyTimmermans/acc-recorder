package be.timsys.gaming.acc.recorder.game.acc;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public abstract class AccMessage {

    private byte[] rawMessage;
    private LittleEndianDataInputStream lil;

    public AccMessage() {}

    public AccMessage(LittleEndianDataInputStream lil, byte[] rawMessage) throws IOException {
        this.rawMessage = rawMessage;
        this.lil = lil;

        build(lil);
    }

    protected abstract void build(LittleEndianDataInputStream lil) throws IOException;


    public static String readString(LittleEndianDataInputStream lil) throws IOException {
        short length = lil.readShort();
        byte[] stringBytes = new byte[length];
        lil.read(stringBytes);
        return new String(stringBytes, StandardCharsets.UTF_8);
    }

    public byte[] getRawMessage() {
        return rawMessage;
    }
}
