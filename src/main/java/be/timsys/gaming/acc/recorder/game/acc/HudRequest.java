package be.timsys.gaming.acc.recorder.game.acc;


import be.timsys.gaming.acc.recorder.game.acc.StructWriter;

import java.io.IOException;

class HudRequest {
    private int connectionId;
    private String hudPage;

    HudRequest(int connectionId, String hudPage) {
        this.connectionId = connectionId;
        this.hudPage = hudPage;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 49);
        structWriter.writeInt(connectionId);
        structWriter.writeString(hudPage);

        return structWriter.toByteArray();
    }
}
