package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class NewSessionEntry {

    private final String sessionId;
    private final SessionEntry sessionEntry;
}
