package be.timsys.gaming.acc.recorder.ui.export;

import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;

import java.util.Map;

public class TextSharePane extends SharePane<Label> {

    public TextSharePane() {
        super(new Label());
    }

    public void setText(String text) {
        getContent().setPrefSize(600, 300);
        getContent().setText(text);
    }

    @Override
    Map<DataFormat, Object> getClipBoardContent(Label content) {
        var clipboardContent = new ClipboardContent();
        clipboardContent.putString(content.getText());
        return clipboardContent;
    }
}
