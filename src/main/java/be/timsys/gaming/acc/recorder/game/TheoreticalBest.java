package be.timsys.gaming.acc.recorder.game;

import lombok.Getter;

import java.util.ArrayList;

@Getter
public class TheoreticalBest extends Lap {

    public void addSplit(Split split) {
        if (splits == null) {
            splits = new ArrayList<>();
        }
        splits.add(split);
        laptimeInMs = splits.stream().mapToInt(Split::getTimeInMilliSec).sum();
    }
}
