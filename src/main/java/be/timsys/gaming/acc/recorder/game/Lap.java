package be.timsys.gaming.acc.recorder.game;

import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import be.timsys.gaming.acc.recorder.game.acc.Weather;
import lombok.*;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lap implements Comparable<Lap> {

    private Integer lapNumber;
    protected Integer laptimeInMs;
    @Builder.Default
    protected List<Split> splits = new ArrayList<>();
    private boolean isValid;
    private boolean isFastest;
    private boolean isInLap;
    private boolean isOutLap;
    private Short positionOnLapCompleted;
    private Weather weather;
    private TimeOfDay gameTimeOfDay;
    private OffsetDateTime realRecordedTime;
    private Driver driver;

    public String getFormattedTime() {
        if (laptimeInMs == null || Integer.MAX_VALUE == laptimeInMs) {
            return "0:00:00";
        } else {
            return DurationFormatUtils.formatDuration(laptimeInMs, "mm:ss.SSS", false);
        }
    }

    public String getFormattedSplits(int splitIndex) {
        if (splitIndex >= this.splits.size() || this.splits.size() == 0) {
            return "0:00.000";
        } else {
            return this.splits.get(splitIndex).getFormattedSplit();
        }
    }

    public Optional<Split> getSplits(int splitIndex) {
        if (splitIndex > this.splits.size() || this.splits.size() == 0) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(this.splits.get(splitIndex));
        }
    }

    @Override
    public int compareTo(Lap o) {
        return this.getLaptimeInMs().compareTo(o.getLaptimeInMs());
    }
}
