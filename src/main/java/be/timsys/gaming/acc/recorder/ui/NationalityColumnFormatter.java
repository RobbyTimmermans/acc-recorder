package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.acc.Nationality;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.util.Callback;

public class NationalityColumnFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    @Override
    public TableCell<S, T> call(TableColumn<S, T> arg0) {
        return new TableCell<>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    var nat = (Nationality) item;
                    var label = new Label(nat.getAbbreviation());
                    label.setTooltip(new Tooltip(nat.getLabel()));
                    setGraphic(label);
                }
            }
        };
    }
}
