package be.timsys.gaming.acc.recorder.ui;

import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

public class TimeOfDayColumnFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    @Override
    public TableCell<S, T> call(TableColumn<S, T> arg0) {
        return new TableCell<>() {
            @Override
            protected void updateItem(T item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setGraphic(null);
                } else {
                    var timeOfDay = (TimeOfDay) item;
                    var label = new Label(timeOfDay.getFormattedTime());
                    setGraphic(label);
                }
            }
        };
    }
}
