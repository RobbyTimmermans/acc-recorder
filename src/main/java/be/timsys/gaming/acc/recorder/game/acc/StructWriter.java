package be.timsys.gaming.acc.recorder.game.acc;

import com.google.common.io.LittleEndianDataOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class StructWriter {
    private final ByteArrayOutputStream outputStream;
    private LittleEndianDataOutputStream littleEndianOutputStream;

    public StructWriter(int size) {
        outputStream = new ByteArrayOutputStream(size);
        littleEndianOutputStream = new LittleEndianDataOutputStream(outputStream);
    }

    public void writeInt(int val) throws IOException {
        littleEndianOutputStream.writeInt(val);
    }

    public void writeString(String val) throws IOException {
        littleEndianOutputStream.writeShort(val.length());
        littleEndianOutputStream.write(val.getBytes(StandardCharsets.UTF_8));
    }

    public void writeByte(Byte b) throws IOException {
        littleEndianOutputStream.writeByte(b);
    }

    public void writeShort(short s) throws IOException {
        littleEndianOutputStream.writeShort(s);
    }


    public byte[] toByteArray() {
        return outputStream.toByteArray();
    }

}
