package be.timsys.gaming.acc.recorder.game.acc;

import java.util.Arrays;

public enum Cup {
    OVERALL((short) 9999),
    PRO_AM((short) 1),
    AM((short) 2),
    SILVER((short) 3);

    private final short id;

    Cup(short id) {
        this.id = id;
    }

    public static Cup fromId(Short id) {
        return Arrays.stream(values()).filter(s -> s.id == id).findFirst().orElse(OVERALL);
    }
}
