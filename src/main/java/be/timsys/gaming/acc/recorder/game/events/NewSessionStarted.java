package be.timsys.gaming.acc.recorder.game.events;

import be.timsys.gaming.acc.recorder.game.acc.SessionType;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class NewSessionStarted {

    private final SessionType sessionType;
    private final short eventIndex;
    private final short sessionIndex;
    private final String trackName;
    private final Integer trackId;
    private final String id;

    public String getFileName() {
        return String.format("dump-%s-%s.acc", sessionType.toString(), id);
    }
}
