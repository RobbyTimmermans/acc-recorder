package be.timsys.gaming.acc.recorder.session;

import be.timsys.gaming.acc.recorder.game.standings.SessionStandings;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;

import javax.persistence.AttributeConverter;

public class JSONConverter implements AttributeConverter<SessionStandings, String> {

    private final ObjectMapper objectMapper;

    JSONConverter() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Override
    @SneakyThrows
    public String convertToDatabaseColumn(SessionStandings entity) {
        return entity != null ? objectMapper.writeValueAsString(entity) : null;
    }

    @Override
    @SneakyThrows
    public SessionStandings convertToEntityAttribute(String json) {
        return json != null ? objectMapper.readValue(json, SessionStandings.class) : null;
    }
}
