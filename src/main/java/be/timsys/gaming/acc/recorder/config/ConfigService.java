package be.timsys.gaming.acc.recorder.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ConfigService {

    private final ObjectMapper objectMapper;

    private Config config;

    @PostConstruct
    private void initPath() throws IOException {
        var dataFolder = Path.of(System.getProperty("user.home"), "acc-sessions");
        if (!Files.exists(dataFolder)) {
            Files.createDirectories(dataFolder);
        }

        this.config = loadConfig(dataFolder)
                .orElseGet(() -> initDefaultConfigAndSave(dataFolder));

    }

    @SneakyThrows
    public void save(Config config) {
        objectMapper.writeValue(getConfigPath(config.getDataFolder()).toFile(), config);
        this.config = config;
    }

    public Config getConfig() {
        return config;
    }

    @SneakyThrows
    private Optional<Config> loadConfig(Path dataFolder) {
        var configFile = getConfigPath(dataFolder);
        if (Files.exists(configFile, LinkOption.NOFOLLOW_LINKS)) {
            return Optional.of(objectMapper.readValue(configFile.toFile(), Config.class));
        } else {
            return Optional.empty();
        }
    }

    private Path getConfigPath(Path dataFolder) {
        return dataFolder.resolve("config.json");
    }

    private Config initDefaultConfigAndSave(Path dataFolder) {
        config = new Config();
        config.setPort(9000);
        config.setServer("localhost");
        config.setExportFolder(dataFolder);
        config.setDataFolder(dataFolder);
        config.setRetryDelayOnGameNotReachableMs(4000);
        config.setReadTimeoutMs(5000);
        config.setUser("asd");
        config.setPwd("");
        config.setInstallationId(UUID.randomUUID().toString());

        save(config);
        return config;
    }
}
