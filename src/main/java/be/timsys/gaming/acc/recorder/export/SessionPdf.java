package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.game.SessionEvent;
import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.DateUtils;
import com.itextpdf.layout.element.Div;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static be.timsys.gaming.acc.recorder.export.PdfUtils.getSplitsWithoutColor;
import static be.timsys.gaming.acc.recorder.export.PdfUtils.getTime;
import static be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType.ACCIDENT;
import static be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType.BEST_SESSION_LAP;

@RequiredArgsConstructor
class SessionPdf {

    private final Session session;

    Paragraph sessionInfo() {
        return new Paragraph(String.format("%s - %s - %s", DateUtils.formatDate(session.getCreated()), session.getSessionType(), session.getTrackName()));
    }

    Div resultsTable() {
        var div = new Div();
        div.add(new Paragraph("Results").setFontSize(15));

        var columnWidths = UnitValue.createPercentArray(new float[]{3, 3, 30, 30, 7, 4});

        Table table = new Table(columnWidths)
                .useAllAvailableWidth()
                .addHeaderCell("P")
                .addHeaderCell("#")
                .addHeaderCell("Drivers")
                .addHeaderCell("Car")
                .addHeaderCell("Fastest")
                .addHeaderCell("Laps");

        session.getSessionStandings().result().forEach(e -> {
            var l = e.getLastLap();
            table
                    .addCell(String.valueOf(l.getPositionOnLapCompleted()))
                    .addCell(e.getNumber())
                    .addCell(e.getDrivers().stream().map(d -> String.format("%s (%s)\n", d.getDriverFullName(), d.getNationality().getAbbreviation())).collect(Collectors.joining()))
                    .addCell(String.format("%s\n%s %s", e.getCar(), e.getTeamName() != null ? e.getTeamName() : "", e.getNationality() != null ? e.getNationality().getAbbreviation() : ""))
                    .addCell(getTime(e.getFastestLap().getFormattedTime(), false))
                    .addCell(String.valueOf(e.getLaps().size()));
        });

        return div.add(table);
    }

    Div fastestLaps(CarClass carClass) {
        var div = new Div();
        div.add(new Paragraph("Fastest laptimes").setFontSize(15));

        var columnWidths = UnitValue.createPercentArray(new float[]{3, 3, 30, 7, 7, 7, 7, 3});

        Table table = new Table(columnWidths)
                .useAllAvailableWidth()
                .addHeaderCell("")
                .addHeaderCell("#")
                .addHeaderCell("Entry")
                .addHeaderCell("Fastest")
                .addHeaderCell("S1")
                .addHeaderCell("S2")
                .addHeaderCell("S3")
                .addHeaderCell("IN");

        var position = new AtomicInteger(0);
        session.getSessionStandings().fastest(carClass).forEach(e -> {
            var l = e.getFastestLap();
            var driver = l.getDriver() != null ? l.getDriver().getDriverFullName() : e.getDriver();
            table
                    .addCell(String.valueOf(position.incrementAndGet()))
                    .addCell(e.getNumber())
                    .addCell(String.format("%s\n%s", driver, e.getCar()))
                    .addCell(getTime(l.getFormattedTime(), false))
                    .addCell(getSplitsWithoutColor(l, 0))
                    .addCell(getSplitsWithoutColor(l, 1))
                    .addCell(getSplitsWithoutColor(l, 2))
                    .addCell(String.valueOf(l.getLapNumber()));
        });

        return div.add(table);
    }

    public Div incidents(CarClass carClass) {
        var incidents = session.getSessionStandings().getSessionEvents(carClass, ACCIDENT);

        var div = new Div().add(new Paragraph("Incidents").setFontSize(15));
        if (!incidents.isEmpty()) {
            div.add(eventsTable(incidents));
        } else {
            div.add(new Paragraph("No incidents"));
        }
        return div;
    }

    public Div bestSessionLaps(CarClass carClass) {
        var bestSessionLaps = session.getSessionStandings().getSessionEvents(carClass, BEST_SESSION_LAP);

        var div = new Div().add(new Paragraph("Best session laps").setFontSize(15));
        if (!bestSessionLaps.isEmpty()) {
            div.add(eventsTable(bestSessionLaps));
        }
        return div;
    }

    private Table eventsTable(List<SessionEvent> events) {
        if (events != null && !events.isEmpty()) {
            var columnWidths = UnitValue.createPercentArray(new float[]{4, 2, 3, 20, 30});
            Table table = new Table(columnWidths)
                    .useAllAvailableWidth()
                    .addHeaderCell("Time")
                    .addHeaderCell("L")
                    .addHeaderCell("D")
                    .addHeaderCell("Event")
                    .addHeaderCell("Description");

            events.forEach(e -> addEvent(table, e));
            return table;
        }
        return null;
    }

    private void addEvent(Table table, SessionEvent e) {
        table.addCell(e.getTimeOfDay().getFormattedTime())
                .addCell(String.valueOf(e.getLapNumber()))
                .addCell(e.getNumber())
                .addCell(String.format("%s\n%s", e.getDriver() != null ? e.getDriver().getDriverFullName() : "", e.getCar()))
                .addCell(e.getDescription());
    }
}
