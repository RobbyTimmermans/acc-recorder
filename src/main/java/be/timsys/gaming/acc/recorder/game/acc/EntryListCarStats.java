package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class EntryListCarStats {

    private short sessionPosition;
    private byte carLocation;
    private BroadCastMessage lastMessage;
    private long totalTime;
    private Integer carLastLapTime = -1;
    private short driverIndex = -1;

    public Optional<BroadCastMessage> getLastMessage() {
        return Optional.ofNullable(lastMessage);
    }
}
