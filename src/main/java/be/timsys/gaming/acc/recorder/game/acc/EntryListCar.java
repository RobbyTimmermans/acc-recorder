package be.timsys.gaming.acc.recorder.game.acc;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@ToString
class EntryListCar extends AccMessage {

    private short carIndex;
    private byte carType;
    private CarType carModelType;
    private String teamName;
    private int raceNumber;
    private byte cupCategory;
    private byte currentDriverIndex;
    private short nationality;
    private byte driversCount;
    private List<Driver> drivers;

    EntryListCar(short carIndex) {
        this.carIndex = carIndex;
    }

    public EntryListCar(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.carIndex = lil.readShort();
        this.carType = lil.readByte();
        this.carModelType = CarType.fromId(carType).orElse(null);
        this.teamName = readString(lil);
        this.raceNumber = lil.readInt();
        this.cupCategory = lil.readByte();
        this.currentDriverIndex = lil.readByte();
        this.nationality = lil.readShort();
        this.driversCount = lil.readByte();
        this.drivers = new ArrayList<>(driversCount);
        for (short i = 0; i < driversCount; i++) {
            drivers.add(new Driver(i, lil));
        }
    }

    protected Driver getCurrentDriver() {
        return drivers != null ? drivers.get(currentDriverIndex) : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntryListCar that = (EntryListCar) o;
        return carIndex == that.carIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(carIndex);
    }

    Driver getDriver(short driverIndex) {
        return drivers != null ? drivers.stream().filter(d -> d.getDriverIndex() == driverIndex).findFirst().orElse(null) : null;
    }
}
