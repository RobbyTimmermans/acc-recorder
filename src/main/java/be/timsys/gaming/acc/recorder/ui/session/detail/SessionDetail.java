package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.export.ExportService;
import be.timsys.gaming.acc.recorder.game.SessionInfo;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.session.SessionService;
import be.timsys.gaming.acc.recorder.ui.DateUtils;
import be.timsys.gaming.acc.recorder.ui.session.SessionInfoBar;
import be.timsys.gaming.acc.recorder.ui.session.TopBar;
import be.timsys.gaming.acc.recorder.ui.session.overview.DeleteSession;
import be.timsys.gaming.acc.recorder.ui.session.overview.SessionOverview;
import be.timsys.javafx.scene.ShowScreenCommand;
import be.timsys.javafx.scene.UiScreen;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import static be.timsys.gaming.acc.recorder.ui.DateUtils.formatDate;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@Component
@RequiredArgsConstructor
public class SessionDetail implements UiScreen<String> {

    private final SessionService sessionService;
    private final ApplicationEventPublisher publisher;
    private final SessionInfoBar sessionInfoBar;
    private final SessionDetailTable sessionStandingsTable;
    private final SessionEventsTable sessionEventsTable;
    private final ExportService exportService;
    private final ExportFileButton export;
    private final Logger logger;

    private Scene scene;
    private TopBar topBar;

    private Session session;

    @Override
    public void init() {
        BorderPane layout = new BorderPane();

        var allSessionsButton = new Button("Back");
        allSessionsButton.setOnAction(this::toOverview);

        var deleteButton = new Button("Delete");
        deleteButton.setOnAction(event -> publisher.publishEvent(DeleteSession.builder().sessionId(session.getId()).build()));

        export.setText("Pdf");
        export.setAction(() -> exportService.exportToPdf(session));

        this.topBar = new TopBar(singletonList(allSessionsButton), "Session detail", asList(export, deleteButton));
        layout.setTop(topBar);

        var center = new BorderPane();
        center.setTop(sessionInfoBar);
        var tabPane = new TabPane(new Tab("Entries", sessionStandingsTable), new Tab("Events", sessionEventsTable));
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        center.setCenter(tabPane);
        layout.setCenter(center);

        this.scene = new Scene(layout);
    }

    @Override
    public void onShow(String sessionId) {
        sessionService.find(sessionId).ifPresent(this::showSession);
    }

    @SneakyThrows
    private void showSession(Session s) {
        this.session = s;
        Platform.runLater(() -> refreshData(s));
    }

    private void refreshData(Session s) {
        logger.debug("Load Session {} - {}", s.getId(), s.getSessionType());

        topBar.setTitle(String.format("%s - %s - %s", formatDate(s.getCreated()), s.getTrackName(), s.getSessionType()));
        sessionInfoBar.setSessionInfo(SessionInfo.builder()
                .trackName(s.getTrackName())
                .sessionType(String.valueOf(s.getSessionType()))
                .build());

        sessionStandingsTable.setData(s.getSessionStandings());
        sessionStandingsTable.setOnDoubleClick(sessionEntry -> publisher.publishEvent(SessionStandingsTableClicked.builder().sessionEntry(sessionEntry).build()));
        sessionEventsTable.setData(session);
    }

    @EventListener
    public void on(SessionStandingsTableClicked event) {
        publisher.publishEvent(new ShowScreenCommand<>(SessionEntryDetail.class.getName(), SessionEntryDetailParam.builder()
                .session(session)
                .sessionEntry(event.getSessionEntry())
                .build()));
    }

    @EventListener
    public void on(DeleteSession deleteSession) {
        sessionService.find(deleteSession.getSessionId()).ifPresent(s -> {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setHeaderText("Delete this session?");
                    alert.setTitle("Delete?");
                    alert.setContentText(String.format("%s - %s - %s", DateUtils.formatDate(s.getCreated()), s.getTrackName(), s.getSessionType()));
                    var result = alert.showAndWait();
                    if (result.filter(b -> b.equals(ButtonType.OK)).isPresent()) {
                        sessionService.delete(deleteSession.getSessionId());
                        toOverview(null);
                    }
                }

        );
    }

    @Override
    public Scene getContent() {
        return scene;
    }

    private void toOverview(ActionEvent event) {
        publisher.publishEvent(new ShowScreenCommand<>(SessionOverview.class.getName()));
    }
}
