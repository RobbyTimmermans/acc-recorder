package be.timsys.gaming.acc.recorder.ui.export;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import lombok.Getter;

import java.util.Map;

@Getter
public abstract class SharePane<T extends Node> extends BorderPane {

    private final T content;
    private final Button shareButton = new Button("Share");
    private final Alert alert;

    public SharePane(T content) {
        this.content = content;

        setCenter(content);
        setBottom(new StackPane(shareButton));

        alert = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);

        shareButton.setOnAction((event -> {
            var clipboard = Clipboard.getSystemClipboard();
            clipboard.setContent(getClipBoardContent(content));
            showShareDone();
        }));
    }

    abstract Map<DataFormat, Object> getClipBoardContent(T content);

    void showShareDone() {
        alert.setContentText(getFinishedText(content));
        alert.show();
    }

    String getFinishedText(T content) {
        return "Copied to clipboard";
    }
}
