package be.timsys.gaming.acc.recorder.game.acc;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum SessionPhase {

    NONE((short) 0),
    STARTING((short) 1),
    PRE_FORMATION((short) 2),
    FORMATION_LAP((short) 3),
    PRE_SESSION((short) 4),
    SESSION((short) 5),
    SESSION_OVER((short) 6),
    POST_SESSION((short) 7),
    RESULT_UI((short) 8);

    private short id;

    SessionPhase(short id) {
        this.id = id;
    }

    public static Optional<SessionPhase> fromId(Short id) {
        return Arrays.stream(values()).filter(s -> s.id == id).findFirst();
    }
}
