package be.timsys.gaming.acc.recorder.ui.export;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;

import java.util.Map;

public class ImageUrlSharePane extends SharePane<ImageView>{

    public ImageUrlSharePane() {
        super(new ImageView());
    }

    public void setImage(Image image) {
        getContent().setImage(image);
    }

    @Override
    protected Map<DataFormat, Object> getClipBoardContent(ImageView content) {
        var clipboardContent = new ClipboardContent();
        clipboardContent.putString(content.getImage().getUrl());
        return clipboardContent;
    }
}
