package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.session.Session;
import lombok.Builder;
import org.apache.commons.lang3.RandomUtils;

import static be.timsys.gaming.acc.recorder.export.ImageExport.encodeString;

@Builder
public class LapImage implements ImageUrl {

    private static final String IMGIX_BASE = "https://acc-recorder.imgix.net/share";

    private final Session session;
    private final Lap lap;
    private final SessionEntry entry;

    private final double width;
    private final double height;

    @Override
    public String getUrl() {
        return String.format("%s/%s?" +
                        "w=%s&h=%s" +
                        "&fit=crop&duotone=3649CC87,7DEC1800&duotone-alpha=94&txt64=%s" +
                        "&txt-color=FFFFFF" +
                        "&txt-size=50" +
                        "&txt-align=middle,center" +
                        "&mark-align=bottom,center&mark64=%s" +
                        "&blend-mode=normal&blend-align=top,center&blend64=%s",
                IMGIX_BASE,
                getBackground(),
                width,
                height,
                encodeString(lap.getFormattedTime()),
                footer(lap, session, entry),
                header(lap, session, entry)
        );
    }

    private String getBackground() {
        return String.format("b%s.png", RandomUtils.nextInt(1, 14));
    }

    private String footer(Lap lap, Session session, SessionEntry entry) {
        var mark = String.format("https://assets.imgix.net/~text?" +
                        "txtpad=10&" +
                        "txtsize=14&" +
                        "w=300&" +
                        "bg=70000000" +
                        "&txtcolor=fff" +
                        "&txt-align=center&txt64=%s",
                encodeString(String.format("Ambient %s, %s, on %s at %s", lap.getWeather().getTempDescription(), lap.getWeather().getWeatherDescription(), lap.getGameTimeOfDay().getFormattedTimeHM(), session.getTrackName())));
        return encodeString(mark);
    }

    private String header(Lap lap, Session session, SessionEntry entry) {
        var blend = String.format("https://assets.imgix.net/~text?" +
                "txtpad=10" +
                "&txtsize=14" +
                "&w=200" +
                "&bg=70000000" +
                "&txtcolor=fff" +
                "&txt-align=center" +
                "&txt64=%s",
                encodeString(String.format("%s %s (%s)", lap.getDriver().getDriverFullName(), entry.getCarType().getDescription(), entry.getCarType().getCarClass())));
        return encodeString(blend);
    }
}
