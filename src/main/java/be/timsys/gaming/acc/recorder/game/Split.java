package be.timsys.gaming.acc.recorder.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.time.DurationFormatUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Split implements Comparable<Split>{

    private Integer index;
    private Integer timeInMilliSec;
    private boolean fastest;

    public String getFormattedSplit() {
        if (Integer.MAX_VALUE == timeInMilliSec) {
            return "0:00.000";
        } else {
            return DurationFormatUtils.formatDuration(timeInMilliSec, "mm:ss.SSS", false);
        }
    }

    @Override
    public int compareTo(Split o) {
        return Integer.compare(timeInMilliSec, o.timeInMilliSec);
    }
}
