package be.timsys.gaming.acc.recorder.ui.session.detail;

import be.timsys.gaming.acc.recorder.export.ExportService;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.session.Session;
import be.timsys.gaming.acc.recorder.ui.session.TopBar;
import be.timsys.javafx.scene.ShowScreenCommand;
import be.timsys.javafx.scene.UiScreen;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static be.timsys.gaming.acc.recorder.ui.DateUtils.formatDate;
import static java.util.Collections.singletonList;

@Component
@RequiredArgsConstructor
public class SessionEntryDetail implements UiScreen<SessionEntryDetailParam> {

    private final ApplicationEventPublisher publisher;
    private final ExportService exportService;
    private final LapTimesTable lapTimesTable;
    private final EntryEventsTable entryEventsTable;
    private final ExportFileButton export;
    private final Logger logger;

    private Scene scene;
    private TopBar topBar;
    private Button back;
    private Label car;

    private Label fastestLap;
    private Label fastestLapS1;
    private Label fastestLapS2;
    private Label fastestLapS3;
    private Label theoLap;
    private Label theoLapS1;
    private Label theoLapS2;
    private Label theoLapS3;
    private GridPane driversGrid;

    @Override
    public void init() {
        var layout = new BorderPane();

        back = new Button("Back");
        export.setText("Pdf");

        this.topBar = new TopBar(singletonList(back), "Session detail", singletonList(export));
        layout.setTop(topBar);

        var sessionInfo = new BorderPane();
        sessionInfo.setTop(new VBox(entryInfo(), createSessionEntryStats()));

        var laps = new TabPane(new Tab("Laps", lapTimesTable), new Tab("Events", entryEventsTable));
        laps.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        sessionInfo.setCenter(laps);

        layout.setCenter(sessionInfo);

        this.scene = new Scene(layout);
    }

    private Node entryInfo() {
        car = new Label();
        car.setStyle("-fx-font-size: 150%; -fx-font-weight: bold");
        car.setPadding(new Insets(10, 10, 10, 10));

        driversGrid = new GridPane();
        driversGrid.setPadding(new Insets(10, 10, 10, 10));
        driversGrid.setHgap(10);
        driversGrid.setVgap(5);

        return new VBox(car, driversGrid);
    }

    private Node createSessionEntryStats() {

        var fastest = new GridPane();
        fastest.setPadding(new Insets(10, 10, 5, 10));
        fastest.setHgap(10);
        fastest.setVgap(5);

        fastestLap = new Label();
        fastestLapS1 = new Label();
        fastestLapS2 = new Label();
        fastestLapS3 = new Label();
        var fastestLabel = new Label("Fastest:");
        fastest.addRow(1, fastestLabel, fastestLap, fastestLapS1, fastestLapS2, fastestLapS3);

        var theo = new GridPane();
        theo.setPadding(new Insets(10, 10, 5, 10));
        theo.setHgap(10);
        theo.setVgap(5);
        theoLap = new Label();
        theoLapS1 = new Label();
        theoLapS2 = new Label();
        theoLapS3 = new Label();
        var theoLabel = new Label("Theoretical best:");
        theo.addRow(1, theoLabel, theoLap, theoLapS1, theoLapS2, theoLapS3);

        GridPane.setHalignment(fastestLabel, HPos.RIGHT);
        GridPane.setHalignment(theoLabel, HPos.RIGHT);

        return new HBox(fastest, theo);
    }

    @Override
    public void onShow(SessionEntryDetailParam param) {
        Platform.runLater(() -> refreshData(param.getSession(), param.getSessionEntry()));
    }

    private void refreshData(Session session, SessionEntry entry) {
        logger.debug("Load Session {} - {}", session.getId(), session.getSessionType());

        back.setOnAction(event -> publisher.publishEvent(new ShowScreenCommand<>(SessionDetail.class.getName(), session.getId())));
        export.setAction(() -> exportService.exportToPdf(session, entry));
        topBar.setTitle(String.format("%s - %s - %s", formatDate(session.getCreated()), session.getTrackName(), session.getSessionType()));

        lapTimesTable.setData(session, entry);
        entryEventsTable.setData(entry);

        car.setText(String.format("#%s %s", entry.getNumber(), entry.getCar()));
        driversGrid.getChildren().clear();
        for (var i = 0; i < entry.getDrivers().size(); i++) {
            var d = entry.getDrivers().get(i);
            var fullName = new Label(String.format("%s - %s (%s - %s)", d.getShortName(), d.getDriverFullName(), d.getNationality().getLabel(), d.getCup()));
            fullName.setStyle("-fx-font-size: 120%");
            driversGrid.addRow(i, fullName);
        }

        var fastestLap = Optional.ofNullable(entry.getFastestLap());
        fastestLap.ifPresent(f -> {
            this.fastestLap.setText(f.getFormattedTime());
            this.fastestLapS1.setText(f.getFormattedSplits(0));
            this.fastestLapS2.setText(f.getFormattedSplits(1));
            this.fastestLapS3.setText(f.getFormattedSplits(2));
        });

        var theoreticalBestLap = Optional.ofNullable(entry.getTheoreticalBestLap());
        theoreticalBestLap.ifPresent(t -> {
            theoLap.setText(t.getFormattedTime());
            theoLapS1.setText(t.getFormattedSplits(0));
            theoLapS2.setText(t.getFormattedSplits(1));
            theoLapS3.setText(t.getFormattedSplits(2));
        });
    }

    @Override
    public Scene getContent() {
        return scene;
    }

}
