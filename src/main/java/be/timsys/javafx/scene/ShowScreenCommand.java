package be.timsys.javafx.scene;

import lombok.Getter;

@Getter
public class ShowScreenCommand<S> {

    private final String screenId;
    private S screenArgument;


    public ShowScreenCommand(String screenId) {
        this.screenId = screenId;
    }

    public ShowScreenCommand(String screenId, S screenArgument) {
        this.screenId = screenId;
        this.screenArgument = screenArgument;
    }
}
