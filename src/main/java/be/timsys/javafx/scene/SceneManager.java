package be.timsys.javafx.scene;

import javafx.stage.Stage;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SceneManager {

    private final Logger logger;
    private final List<UiScreen<?>> screens;

    private Stage primaryStage;

    public void start() {
        logger.debug("Initializing all screens");
        screens.forEach(UiScreen::init);

        showScene(screens.stream()
                        .filter(UiScreen::isStartPage)
                        .findFirst().orElseThrow(() -> new RuntimeException("No Start screen defined")),
                null);
    }

    @EventListener
    public void onShowScreen(ShowScreenCommand<?> command) {
        logger.debug("Request to show screen {}", command.getScreenId());
        showScene(screens.stream()
                        .filter(sc -> sc.getId().equalsIgnoreCase(command.getScreenId()))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException(String.format("No screen with id %s found", command.getScreenId()))),
                command.getScreenArgument());
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void showScene(UiScreen uiScreen, Object argument) {
        logger.debug("Showing screen {} with argument {}", uiScreen.getId(), argument);
        uiScreen.onShow(argument);
        primaryStage.setScene(uiScreen.getContent());
    }
}
