package be.timsys.javafx.scene;

import javafx.scene.Scene;

public interface UiScreen<S> {

    Scene getContent();

    void init();

    default boolean isStartPage() {
        return false;
    };

    default String getId() {
        return this.getClass().getName();
    }

    default void onShow(S argument) {
    }
}
