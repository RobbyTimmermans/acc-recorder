create table session
(
    id           varchar(40) not null primary key,
    created      timestamp   not null default now(),
    lastModified timestamp   not null default now(),
    trackName    varchar(255),
    sessionType varchar(150),
    fileName varchar(255),
    sessionStandings clob
);
