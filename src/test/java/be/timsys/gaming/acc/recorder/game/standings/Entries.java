package be.timsys.gaming.acc.recorder.game.standings;

import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.acc.CarType;

import static be.timsys.gaming.acc.recorder.game.acc.CarType.*;
import static be.timsys.gaming.acc.recorder.game.standings.Drivers.*;
import static java.util.Collections.singletonList;

public class Entries {

    public SessionEntry PORSCHE_75 = SessionEntry.builder()
            .id((short) 1000)
            .position((short) 1)
            .currentDriver(ROBBY)
            .drivers(singletonList(ROBBY))
            .number("75")
            .car(PORSCHE_991_GT3_R.getDescription())
            .carType(PORSCHE_991_GT3_R)
            .build();

    public SessionEntry FERRARI_33 = SessionEntry.builder()
            .currentDriver(JOSY)
            .drivers(singletonList(JOSY))
            .id((short) 1002)
            .position((short) 2)
            .number("33")
            .car(CarType.FERRARI_488_GT3.getDescription())
            .carType(FERRARI_488_GT3)
            .build();

    public SessionEntry MERCEDES_66 = SessionEntry.builder()
            .currentDriver(AYRTON)
            .drivers(singletonList(AYRTON))
            .id((short) 1003)
            .position((short) 3)
            .number("66")
            .car(CarType.MERCEDES_AMG_GT3_EVO.getDescription())
            .carType(MERCEDES_AMG_GT3_EVO)
            .build();

    public SessionEntry AUDI_15 = SessionEntry.builder()
            .currentDriver(LEWIS)
            .drivers(singletonList(LEWIS))
            .id((short) 1004)
            .position((short) 4)
            .number("15")
            .car(CarType.AUDI_R8_LMS_EVO.getDescription())
            .carType(AUDI_R8_LMS_EVO)
            .build();

    public SessionEntry NISSAN_3 = SessionEntry.builder()
            .currentDriver(NIGEL)
            .drivers(singletonList(NIGEL))
            .id((short) 1005)
            .position((short) 5)
            .number("3")
            .car(CarType.NISSAN_GT_R_NISMO_GT3_2018.getDescription())
            .carType(NISSAN_GT_R_NISMO_GT3_2018)
            .build();

    public SessionEntry JAGUAR_9 = SessionEntry.builder()
            .currentDriver(JEAN_MICHEL)
            .drivers(singletonList(JEAN_MICHEL))
            .id((short) 1006)
            .position((short) 5)
            .number("9")
            .car(EMIL_FREY_JAGUAR_GT3.getDescription())
            .carType(EMIL_FREY_JAGUAR_GT3)
            .build();

    public SessionEntry PORSCHE_CAYMAN_123 = SessionEntry.builder()
            .currentDriver(MARINA_TOURNEDOS)
            .drivers(singletonList(MARINA_TOURNEDOS))
            .id((short) 1007)
            .position((short) 6)
            .number("123")
            .car(PORSCHE_718_CAYMAN_GT4_CLUBSPORT.getDescription())
            .carType(PORSCHE_718_CAYMAN_GT4_CLUBSPORT)
            .build();
}
