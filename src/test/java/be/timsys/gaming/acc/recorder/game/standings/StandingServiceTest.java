package be.timsys.gaming.acc.recorder.game.standings;

import be.timsys.gaming.acc.recorder.MockPublisher;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType;
import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import be.timsys.gaming.acc.recorder.game.events.BroadCastEvent;
import be.timsys.gaming.acc.recorder.game.events.NewSessionEntry;
import be.timsys.gaming.acc.recorder.game.events.StatusChanged;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class StandingServiceTest {

    private StandingService standingService;
    private MockPublisher publisher;
    private SessionEntry audi;

    @Before
    public void setUp() {
        Logger logger = LoggerFactory.getLogger(StandingService.class);
        publisher = new MockPublisher();

        standingService = new StandingService(publisher, logger);

        Entries entries = new Entries();
        audi = entries.AUDI_15;
        standingService.on(new NewSessionEntry("123", audi));

    }

    @Test
    public void testOnBroadCastEvent() {
        standingService.on(BroadCastEvent.builder()
                .description("1:44")
                .carIndex(audi.getId())
                .realTime(OffsetDateTime.now())
                .timeOfDay(new TimeOfDay(50000.0))
                .type(BroadCastMessageType.BEST_SESSION_LAP)
                .sessionId("123")
                .build());

        assertThat(publisher.getPublishedObjects(StatusChanged.class))
                .isNotNull().hasSize(1)
        .extracting("status")
        .contains("Best Session Lap reported for #15 - Lewis Hamerton in car Audi R8 LMS Evo");
    }
}
