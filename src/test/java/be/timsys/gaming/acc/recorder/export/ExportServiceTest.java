package be.timsys.gaming.acc.recorder.export;

import be.timsys.gaming.acc.recorder.config.ConfigService;
import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.Split;
import be.timsys.gaming.acc.recorder.game.acc.SessionType;
import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import be.timsys.gaming.acc.recorder.game.acc.Weather;
import be.timsys.gaming.acc.recorder.game.standings.Entries;
import be.timsys.gaming.acc.recorder.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

@RunWith(MockitoJUnitRunner.class)
public class ExportServiceTest {

    private ExportService exportService;

    @Mock
    private ConfigService configService;

    private SessionEntry audi;
    private OffsetDateTime sessionDate;
    private Session session;
    private Lap lap;

    @Before
    public void setup() {
        exportService = new ExportService(configService, new ImageExport());

        var entries = new Entries();
        audi = entries.AUDI_15;

        sessionDate = OffsetDateTime.of(2021, 10, 24, 13, 55, 22, 0, UTC);
        session = Session.builder()
                .sessionType(SessionType.QUALIFYING)
                .created(sessionDate)
                .trackName("Zolder").build();

        lap = Lap.builder()
                .driver(audi.getCurrentDriver())
                .gameTimeOfDay(new TimeOfDay(5000.0))
                .lapNumber(10)
                .laptimeInMs(90000)
                .positionOnLapCompleted((short) 5)
                .weather(new Weather(0, 0, 0, 20, 25))
                .splits(List.of(new Split(0, 30000, false),
                        new Split(1, 40000, false),
                        new Split(2, 20000, false)))
                .build();
    }

    @Test
    public void testExportToString() {
        var lapString = exportService.exportToString(session, audi, lap);

        assertThat(lapString).isEqualTo(
                "Sun, 24 Oct - 13:55 - Zolder - Lewis Hamerton - Audi R8 LMS Evo (GT3)\n" +
                        "Lap Time 1:30.000 (S1: 0:30.000, S2: 0:40.000, S3: 0:20.000)\n" +
                        "20C - Track 25C - dry (0) - 0% clouds - 0% wetness\n" +
                        "Time of day 1:23");
    }

    @Test
    public void testExportToImageUrl() throws MalformedURLException {
        var urlString = exportService.exportToImageUrl(session, audi, lap, 500, 500);
        assertThat(urlString).isNotNull();

        var url = new URL(urlString);

        var params = new HashMap<String, String>();
        Arrays.stream(StringUtils.split(url.getQuery(), '&'))
                .map(s -> StringUtils.split(s, '='))
                .forEach(array -> params.put(array[0], array[1]));

        assertThat(params).contains(
                entry("w", "500.0"),
                entry("h", "500.0")
        );

        var lapTime = new String(Base64.getDecoder().decode(params.get("txt64")), StandardCharsets.UTF_8);
        assertThat(lapTime).isEqualTo(lap.getFormattedTime());
    }
}
