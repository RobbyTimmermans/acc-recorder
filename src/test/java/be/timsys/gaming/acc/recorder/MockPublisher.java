package be.timsys.gaming.acc.recorder;

import org.springframework.context.ApplicationEventPublisher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MockPublisher implements ApplicationEventPublisher {

    private List<Object> publishedObjects;

    public MockPublisher() {
        this.publishedObjects = new ArrayList<>();
    }

    @Override
    public void publishEvent(Object event) {
        publishedObjects.add(event);
    }

    public List<Object> getPublishedObjects(Class<?> ofClazz) {
        return publishedObjects.stream().filter(o -> o.getClass().isAssignableFrom(ofClazz)).collect(Collectors.toList());
    }

    public void clear() {
        publishedObjects.clear();
    }
}
