package be.timsys.gaming.acc.recorder.session;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.Split;
import be.timsys.gaming.acc.recorder.game.standings.SessionStandings;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
public class JSONConverterTest {

    private JSONConverter jsonConverter;

    @Before
    public void setUp() {
        jsonConverter = new JSONConverter();
    }

    @Test
    public void testFromJson() throws IOException, URISyntaxException {
        var resource = getClass().getResource("/standings.json");

        String json = Files.readString(Paths.get(resource.toURI()), StandardCharsets.UTF_8);

        var sessionStandings = jsonConverter.convertToEntityAttribute(json);
        assertThat(sessionStandings).isNotNull();
    }

    @Test
    public void testToJson() {
        var sessionStandings = new SessionStandings();
        var entry = SessionEntry.builder()
                .id((short) 1)
                .car("Car")
                .number("1")
                .position((short) 25)
                .driver("Jos")
                .build();
        entry.addLap(Lap.builder()
                .laptimeInMs(1555)
                .splits(Arrays.asList(new Split(0, 55, false), new Split(0, 55, false), new Split(0, 55, false)))
                .build());

        sessionStandings.addEntry(entry);

        var json = jsonConverter.convertToDatabaseColumn(sessionStandings);
        assertThat(json).isNotNull();

        var sessionStandings1 = jsonConverter.convertToEntityAttribute(json);
        assertThat(sessionStandings).usingRecursiveComparison().isEqualTo(sessionStandings1);
    }
}
