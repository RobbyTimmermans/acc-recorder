package be.timsys.gaming.acc.recorder.game.standings;

import be.timsys.gaming.acc.recorder.game.Lap;
import be.timsys.gaming.acc.recorder.game.SessionEntry;
import be.timsys.gaming.acc.recorder.game.SessionEvent;
import be.timsys.gaming.acc.recorder.game.Split;
import be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType;
import be.timsys.gaming.acc.recorder.game.acc.CarClass;
import be.timsys.gaming.acc.recorder.game.acc.TimeOfDay;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;

import java.time.OffsetDateTime;
import java.util.Arrays;

import static be.timsys.gaming.acc.recorder.game.acc.BroadCastMessageType.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

public class SessionStandingsTest {

    private SessionStandings sessionStandings;
    private Entries entries;

    @Before
    public void setUp() {
        entries = new Entries();
        sessionStandings = new SessionStandings();

        sessionStandings.addEntry(entries.PORSCHE_75);
        sessionStandings.addEntry(entries.FERRARI_33);
        sessionStandings.addEntry(entries.MERCEDES_66);
        sessionStandings.addEntry(entries.NISSAN_3);
        sessionStandings.addEntry(entries.AUDI_15);
        sessionStandings.addEntry(entries.JAGUAR_9);
        sessionStandings.addEntry(entries.PORSCHE_CAYMAN_123);

        sessionStandings.updatePosition(entries.PORSCHE_75.getId(), (short) 1);
        sessionStandings.addLap(entries.PORSCHE_75.getId(), getLap(entries.PORSCHE_75, 1, 1));
        sessionStandings.addLap(entries.PORSCHE_75.getId(), getLap(entries.PORSCHE_75, 2, 1, 300, 500, 200));
        sessionStandings.addLap(entries.PORSCHE_75.getId(), getLap(entries.PORSCHE_75, 3, 1));
        sessionStandings.addLap(entries.PORSCHE_75.getId(), getLap(entries.PORSCHE_75, 4, 1));

        sessionStandings.updatePosition(entries.FERRARI_33.getId(), (short) 3);
        sessionStandings.addLap(entries.FERRARI_33.getId(), getLap(entries.FERRARI_33, 1, 3));
        sessionStandings.updatePosition(entries.FERRARI_33.getId(), (short) 2);
        sessionStandings.addLap(entries.FERRARI_33.getId(), getLap(entries.FERRARI_33, 2, 2));
        sessionStandings.addLap(entries.FERRARI_33.getId(), getLap(entries.FERRARI_33, 3, 2, 300, 499, 200));
        sessionStandings.updatePosition(entries.FERRARI_33.getId(), (short) 3);
        sessionStandings.addLap(entries.FERRARI_33.getId(), getLap(entries.FERRARI_33, 4, 3));

        sessionStandings.updatePosition(entries.NISSAN_3.getId(), (short) 2);
        sessionStandings.addLap(entries.NISSAN_3.getId(), getLap(entries.NISSAN_3, 1, 2));
        sessionStandings.updatePosition(entries.NISSAN_3.getId(), (short) 3);
        sessionStandings.addLap(entries.NISSAN_3.getId(), getLap(entries.NISSAN_3, 2, 3));
        sessionStandings.updatePosition(entries.NISSAN_3.getId(), (short) 4);
        sessionStandings.addLap(entries.NISSAN_3.getId(), getLap(entries.NISSAN_3, 3, 4, 300, 501, 200));
        sessionStandings.addLap(entries.NISSAN_3.getId(), getLap(entries.NISSAN_3, 4, 4));

        sessionStandings.updatePosition(entries.AUDI_15.getId(), (short) 4);
        sessionStandings.addLap(entries.AUDI_15.getId(), getLap(entries.AUDI_15, 1, 4));
        sessionStandings.addLap(entries.AUDI_15.getId(), getLap(entries.AUDI_15, 2, 4));
        sessionStandings.updatePosition(entries.AUDI_15.getId(), (short) 3);
        sessionStandings.addLap(entries.AUDI_15.getId(), getLap(entries.AUDI_15, 3, 3));
        sessionStandings.updatePosition(entries.AUDI_15.getId(), (short) 2);
        sessionStandings.addLap(entries.AUDI_15.getId(), getLap(entries.AUDI_15, 4, 2, 305, 500, 200));

        sessionStandings.updatePosition(entries.JAGUAR_9.getId(), (short) 1);
        sessionStandings.addLap(entries.JAGUAR_9.getId(), getLap(entries.JAGUAR_9, 1, 1, 306, 500, 200));

        addEvent(entries.AUDI_15, BEST_PERSONAL_LAP, 5000d, "BPL", 1);
        addEvent(entries.AUDI_15, BEST_SESSION_LAP, 6000d, "BPS", 1);
        addEvent(entries.AUDI_15, ACCIDENT, 5020d, "ACC", 1);
        addEvent(entries.JAGUAR_9, BEST_PERSONAL_LAP, 7000d, "BPL", 2);
        addEvent(entries.FERRARI_33, BEST_SESSION_LAP, 7050d, "BPS", 3);
    }

    @Test
    public void testFastest() {
        var fastest = sessionStandings.fastest(CarClass.GT3);

        assertThat(fastest).isNotNull().hasSize(5)
                .extracting("id").containsExactly(entries.FERRARI_33.getId(), entries.PORSCHE_75.getId(), entries.NISSAN_3.getId(), entries.AUDI_15.getId(), entries.JAGUAR_9.getId());
    }

    @Test
    public void testResult() {
        var result = sessionStandings.result();

        assertThat(result).isNotNull().hasSize(7)
                .extracting("id", "car", "position", "numberOfLaps")
                .containsExactly(
                        tuple(entries.PORSCHE_75.getId(), entries.PORSCHE_75.getCar(), (short) 1, 4),
                        tuple(entries.AUDI_15.getId(), entries.AUDI_15.getCar(), (short) 2, 4),
                        tuple(entries.FERRARI_33.getId(), entries.FERRARI_33.getCar(), (short) 3, 4),
                        tuple(entries.NISSAN_3.getId(), entries.NISSAN_3.getCar(), (short) 4, 4),
                        tuple(entries.JAGUAR_9.getId(), entries.JAGUAR_9.getCar(), (short) 1, 1),
                        tuple(entries.MERCEDES_66.getId(), entries.MERCEDES_66.getCar(), (short) 3, 0),
                        tuple(entries.PORSCHE_CAYMAN_123.getId(), entries.PORSCHE_CAYMAN_123.getCar(), (short) 6, 0)
                );
    }

    @Test
    public void testGetEvents() {
        var bestSessionLaps = sessionStandings.getSessionEvents(CarClass.GT3, BEST_SESSION_LAP);
        assertThat(bestSessionLaps).isNotNull().hasSize(2)
                .extracting("number", "type", "lapNumber")
                .containsExactly(tuple(entries.AUDI_15.getNumber(), BEST_SESSION_LAP, 1),
                        tuple(entries.FERRARI_33.getNumber(), BEST_SESSION_LAP, 3));
    }

    @Test
    public void getCarClasses() {
        assertThat(sessionStandings.getCarClasses()).containsExactly(CarClass.GT3, CarClass.GT4);

        var gt4 = sessionStandings.getSessionEntries().stream().filter(s -> s.getCarClass() == CarClass.GT4).findFirst().orElse(null);
        sessionStandings.getSessionEntries().remove(gt4);

        assertThat(sessionStandings.getCarClasses()).containsExactly(CarClass.GT3);
    }

    private Lap getLap(SessionEntry entry, int lapNumber, int position) {
        var s1 = RandomUtils.nextInt(320, 340);
        var s2 = RandomUtils.nextInt(510, 550);
        var s3 = RandomUtils.nextInt(210, 250);

        return getLap(entry, lapNumber, position, s1, s2, s3);
    }

    private Lap getLap(SessionEntry entry, int lapNumber, int position, int s1, int s2, int s3) {
        var totalTime = s1 + s2 + s3;

        return Lap.builder()
                .driver(entry.getCurrentDriver())
                .isValid(true)
                .positionOnLapCompleted((short) position)
                .lapNumber(lapNumber)
                .laptimeInMs(totalTime)
                .splits(Arrays.asList(new Split(0, s1, false), new Split(0, s2, false), new Split(0, s3, false)))
                .build();
    }

    private void addEvent(SessionEntry entry, BroadCastMessageType type, double time, String description, int lapNumber) {
        sessionStandings.addEvent(entry.getId(), SessionEvent.builder()
                .number(entry.getNumber())
                .driver(entry.getCurrentDriver())
                .type(type)
                .timeOfDay(new TimeOfDay(time))
                .realTime(OffsetDateTime.now())
                .description(description)
                .lapNumber(lapNumber)
                .build());
    }
}
