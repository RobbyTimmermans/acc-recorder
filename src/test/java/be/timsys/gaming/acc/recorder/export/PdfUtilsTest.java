package be.timsys.gaming.acc.recorder.export;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PdfUtilsTest {

    @Test
    public void makeFileNameUsable() {
        assertThat(PdfUtils.makeFileNameUsable("Circuit de Spa Francorchamps")).isEqualTo("circuit_de_spa_francorchamps");
    }
}
