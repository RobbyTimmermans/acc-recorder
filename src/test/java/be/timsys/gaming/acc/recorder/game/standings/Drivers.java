package be.timsys.gaming.acc.recorder.game.standings;

import be.timsys.gaming.acc.recorder.game.Driver;

import static be.timsys.gaming.acc.recorder.game.acc.Cup.*;
import static be.timsys.gaming.acc.recorder.game.acc.Nationality.*;

public class Drivers {

    static Driver ROBBY = Driver.builder().driverFullName("Robby Timmermans")
                .nationality(Belgium)
                .cup(OVERALL)
                .shortName("RTI")
                .lastName("Timmermans")
                .firstName("Robby").build();

    static Driver AYRTON = Driver.builder().driverFullName("Ayrton Sonny")
                .nationality(Brazil)
                .cup(PRO_AM)
                .shortName("AYS")
                .lastName("Sonny")
                .firstName("Ayrton").build();

    static Driver JOSY = Driver.builder().driverFullName("Josy Wosy")
                .nationality(Nepal)
                .cup(OVERALL)
                .shortName("JWO")
                .lastName("Wosy")
                .firstName("Josy").build();

    static Driver LEWIS = Driver.builder().driverFullName("Lewis Hamerton")
            .nationality(GreatBritain)
            .cup(OVERALL)
            .shortName("LWH")
            .lastName("Hamerton")
            .firstName("Lewis").build();

    static Driver NIGEL = Driver.builder().driverFullName("Nigel Mamsel")
            .nationality(USA)
            .cup(OVERALL)
            .shortName("NMA")
            .lastName("Mamsel")
            .firstName("Nigel").build();

    static Driver JEAN_MICHEL = Driver.builder().driverFullName("Jean Michel Fansio")
            .nationality(Italy)
            .cup(OVERALL)
            .shortName("JMI")
            .lastName("Fansio")
            .firstName("Jean Michel").build();

    static Driver MARINA_TOURNEDOS = Driver.builder().driverFullName("Marina Tournedos")
            .nationality(France)
            .cup(OVERALL)
            .shortName("MTO")
            .lastName("Tournedos")
            .firstName("Marina").build();
}
